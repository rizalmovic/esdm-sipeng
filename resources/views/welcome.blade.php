@extends('layouts.main')

@section('content')

	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="panel-title">USERS SAMPLE DEMO - Informasi ini akan dihapus saat live.</div>
		</div>
		<table class="table">
			<thead>
				<tr>
					<th>Username</th>
					<th>Password</th>
					<th>Satuan Kerja</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>user@example.net</td>
					<td>abc123</td>
					<td>Sekretariat Badan Penelitian Dan Pengembangan Energi Dan Sumber Daya Mineral</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="panel-title">Statistik Pengadaan Penyedia - Tahun Anggaran 2016</div>
		</div>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th width="500" style="text-align:center;vertical-align:middle" rowspan="2">Satuan Kerja</th>
					<th style="text-align:center;vertical-align:middle" colspan="4">Penyedia</th>
				</tr>
				<tr>
					<th style="text-align:center;vertical-align:middle">Total</th>
					<th style="text-align:center;vertical-align:middle">Lelang</th>
					<th style="text-align:center;vertical-align:middle">Kontrak</th>
					<th style="text-align:center;vertical-align:middle">Selesai</th>
				</tr>
			</thead>
			<tbody>
				@foreach($penyedia['retval'] as $s)
					<tr>
						<td>{{ucwords(strtolower($s['satuan_kerja']))}}</td>
						<td align="center">{{$s['total_paket']}}</td>
						<td align="center"><span class="text-success">{{$s['lelang']}}</span> / <span class="text-danger">{{$s['belum_lelang']}}</span></td>
						<td align="center"><span class="text-success">{{$s['kontrak']}}</span> / <span class="text-danger">{{$s['belum_kontrak']}}</span></td>
						<td align="center"><span class="text-success">{{$s['selesai']}}</span> / <span class="text-danger">{{$s['belum_selesai']}}</span></td>
					</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<th>Total</th>
					<td align="center">{{$penyedia['count']}}</td>
					<td align="center">
						<span class="text-success">{{array_sum(array_pluck($penyedia['retval'],'lelang'))}}</span> / 
						<span class="text-danger">{{array_sum(array_pluck($penyedia['retval'],'belum_lelang'))}}</span>
					</td>
					<td align="center">
						<span class="text-success">{{array_sum(array_pluck($penyedia['retval'],'kontrak'))}}</span> / 
						<span class="text-danger">{{array_sum(array_pluck($penyedia['retval'],'belum_kontrak'))}}</span>
					</td>
					<td align="center">
						<span class="text-success">{{array_sum(array_pluck($penyedia['retval'],'selesai'))}}</span> / 
						<span class="text-danger">{{array_sum(array_pluck($penyedia['retval'],'belum_selesai'))}}</span>
					</td>
				</tr>
			</tfoot>
		</table>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="panel-title">Statistik Pengadaan Swakelola - Tahun Anggaran 2016</div>
		</div>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th width="500" style="text-align:center;vertical-align:middle" rowspan="2">Satuan Kerja</th>
					<th style="text-align:center;vertical-align:middle" colspan="3">Swakelola</th>
				</tr>
				<tr>
					<th style="text-align:center;vertical-align:middle">Total</th>
					<th style="text-align:center;vertical-align:middle">Kontrak</th>
					<th style="text-align:center;vertical-align:middle">Selesai</th>
				</tr>
			</thead>
			<tbody>
				@foreach($swakelola['retval'] as $s)
					<tr>
						<td>{{ucwords(strtolower($s['satuan_kerja']))}}</td>
						<td align="center">{{$s['total_paket']}}</td>
						<td align="center"><span class="text-success">{{$s['kontrak']}}</span> / <span class="text-danger">{{$s['belum_kontrak']}}</span></td>
						<td align="center"><span class="text-success">{{$s['selesai']}}</span> / <span class="text-danger">{{$s['belum_selesai']}}</span></td>
					</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<th>Total</th>
					<td align="center">{{$swakelola['count']}}</td>
					<td align="center">
						<span class="text-success">{{array_sum(array_pluck($swakelola['retval'],'kontrak'))}}</span> / 
						<span class="text-danger">{{array_sum(array_pluck($swakelola['retval'],'belum_kontrak'))}}</span>
					</td>
					<td align="center">
						<span class="text-success">{{array_sum(array_pluck($swakelola['retval'],'selesai'))}}</span> / 
						<span class="text-danger">{{array_sum(array_pluck($swakelola['retval'],'belum_selesai'))}}</span>
					</td>
				</tr>
			</tfoot>
		</table>
	</div>
@endsection