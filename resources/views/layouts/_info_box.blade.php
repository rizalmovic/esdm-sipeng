<ul class="list-group">
	<li class="list-group-item">
		<img src="{{asset('/img/logo.png')}}" width="100" alt="logo" class="img-thumbnail img-circle center-block">
	</li>
	<li class="list-group-item" style="background: rgb(249,241,56);color: #000;border-top-color: rgb(249,241,56);border-bottom-color: rgb(249,241,56)">
		<strong style="text-align:center !important;display:block">{{date('d M Y', strtotime('now'))}}</strong>
	</li>
	<a href="{{route('index')}}" class="list-group-item"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
	<a href="{{route('artikel.index')}}" class="list-group-item"><i class="fa fa-fw fa-file-text-o"></i> Artikel</a>
	<a href="{{route('laporan.index')}}" class="list-group-item"><i class="fa fa-fw fa-line-chart"></i> Laporan</a>
	<li class="list-group-item disabled">Rencana</li>
	<a class="list-group-item" href="{{route('paket.penyedia.rencana')}}">Penyedia</a>
	<a class="list-group-item" href="{{route('paket.swakelola.rencana')}}">Swakelola</a>
	<li class="list-group-item disabled">Realisasi</li>
	<a class="list-group-item" href="{{route('paket.penyedia.realisasi')}}">Penyedia</a>
	<a class="list-group-item" href="{{route('paket.swakelola.realisasi')}}">Swakelola</a>

	@if(isset($params['title']))
		@if($params['title'] == 'Penyedia' || $params['title'] == 'Swakelola')
		<li class="list-group-item disabled">Filter</li>
		<li class="list-group-item">
			@include('core::paket._filters')
		</li>
		@endif
	@endif
</ul>