<link rel="stylesheet" href="{{asset('css/app.css')}}">
<link rel="stylesheet" href="{{asset('css/selectize.css')}}">
<link rel="stylesheet" href="{{asset('css/selectize.bootstrap3.css')}}">
<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('css/ionicons.min.css')}}">
@yield('styles')