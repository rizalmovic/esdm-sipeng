<header>
  <h1>
    <a href="{{route('index')}}">
      <img src="{{asset('img/logo-long.png')}}">
    </a>
  </h1>
</header>
<nav class="navbar navbar-blue navbar-fixed-top">
  <div class="container-fluid">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="javascript:;">SIPENG v2.0 - Badan Litbang ESDM</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse">
      <ul class="nav navbar-nav navbar-right">
        @if(Sentinel::getUser() && Sentinel::getUser()->hasAccess('admin'))
        <li class="dropdown">
          <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-fw fa-cog"></i> Pengaturan <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="{{route('pengguna.index')}}"><i class="fa fa-fw ion-ios-people"></i> Pengguna</a></li>
            <li><a href="{{route('satker.index')}}"><i class="fa fa-fw fa-briefcase"></i> Satuan Kerja</a></li>
          </ul>
        </li>
        @endif
        @if(Sentinel::check())
          <li><a href="{{route('profile')}}"><i class="fa fa-fw fa-male"></i> My Profile</a></li>
        	<li><a href="{{route('logout')}}"><i class="fa fa-fw fa-sign-out"></i> Logout</a></li>
        @else
        	<li><a href="{{route('login')}}"><i class="fa fa-fw fa-sign-in"></i> Login</a></li>
        @endif
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>