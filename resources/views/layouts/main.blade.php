<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>SIPENG</title>
	@include('layouts._styles')
</head>
<body>
	@include('layouts._navbar')

	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-md-3 col-lg-2">
				@include('layouts._info_box')
			</div>
			<div class="col-xs-12 col-md-9 col-lg-10">
				@yield('content')
			</div>
		</div>
	</div>

	@include('layouts._scripts')
</body>
</html>