<script src="{{asset('js/jquery-1.11.3.min.js')}}"></script>
<script src="{{asset('js/moment.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/selectize.min.js')}}"></script>
<script src="{{asset('js/bootbox.min.js')}}"></script>
<script src="{{asset('js/app.js')}}"></script>
@yield('scripts')