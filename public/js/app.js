$(function(){
	var App = {
		plugins: {
			selectize: function(){
				$('[data-selectize]').each(function(){
					var options = $(this).data('options') || {};
					console.log(options);
					$(this).selectize(options);
				});
			},
			tableDetail: function(){
				$('.table-detail').find('a.show-detail-btn').on('click', function(){
					$(this).find('i').toggleClass('fa-caret-right');
					$(this).parent().parent().next().toggleClass('hide');
				});
			}
		},
		init: function(){
			App.plugins.selectize();
			App.plugins.tableDetail();
		}
	};

	// SELECTIZE
	App.init();
});