$(function(){
	$.fn.editable.defaults.mode = 'popup';
	$('.edit').editable({
		success: function(){
			bootbox.alert('Data telah berhasil disimpan.');
			// window.location.reload();
		},
		error: function(res){
			bootbox.alert(res.status);
		}
	});
});