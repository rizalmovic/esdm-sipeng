var webpage = require('webpage'),
	penyediaURL = "https://sirup.lkpp.go.id/sirup/datatablectr/dataruppenyediasatker?tahun={{tahun}}&idSatker={{kode_satker}}&iDisplayLength=1000",
	penyediaDetilURL = "https://sirup.lkpp.go.id/sirup/rup/detailPaketPenyedia/{{paket_id}}",
	sipengURL = "http://sipeng.esdm.dev/paket/penyedia",
	system = require('system'),
	target = {}, satker, tahun, result, onProgress = false;

var page = webpage.create(),
	detilPage = webpage.create();

if(system.args.length < 2) {
	console.log('Invalid requests!');
	phantom.exit();
} else {
	satker = system.args[1];
	tahun  = system.args[2] || new Date().getFullYear();
	target.penyedia = penyediaURL.replace('{{kode_satker}}',satker).replace('{{tahun}}',tahun);
	target.swakelola = swakelolaURL.replace('{{kode_satker}}',satker).replace('{{tahun}}',tahun);
	console.log(target.penyedia);
	page.open(target.penyedia, function(status) {
		result = JSON.parse(page.plainText).aaData;
		if(status !== 'success') {
			console.log('Unable access network!');
		} else {
			var pageIndex = 0;

			var indexInterval = setInterval(function(){
				if(!onProgress && pageIndex < result.length) {
					var detilUrl = penyediaDetilURL.replace('{{paket_id}}', result[pageIndex][0]);
					page.open(detilUrl);
				}

				if(pageIndex == result.length) {
					console.log('finished!');
					phantom.exit();
				}
			}, 250);

			page.onLoadStarted = function() {
			    onProgress = true;
			};
			page.onLoadFinished = function(status){
				onProgress = false;
				// page.injectJs('https://code.jquery.com/jquery-1.11.3.min.js');
				if(status !== 'success') {
					console.log('Unable access network!');
				} else {
					var detil = page.evaluate(function(){
						var attributes = {},
							divs = $('.form-wrap').find('div').slice(1);

						attributes.kldi = divs.eq(0).clone().children().remove().end().text().trim();
						attributes.satuan_kerja = divs.eq(1).clone().children().remove().end().text().trim();
						attributes.tahun_anggaran = divs.eq(2).clone().children().remove().end().text().trim();
						attributes.kegiatan = divs.eq(3).clone().children().remove().end().text().trim();
						attributes.paket_nama = divs.eq(4).clone().children().remove().end().text().trim();
						attributes.paket_id = divs.eq(5).clone().children().remove().end().text().trim();
						attributes.jenis_belanja = divs.eq(6).clone().children().remove().end().text().trim();
						attributes.volume = divs.eq(8).clone().children().remove().end().text().trim();
						attributes.deskripsi = divs.eq(9).clone().children().remove().end().text().trim();
						attributes.tanggal_pengumuman = divs.eq(10).clone().children().remove().end().text().trim();
						attributes.sumber_dana = divs.eq(11).clone().children().remove().end().text().trim();
						attributes.pagu = divs.eq(12).find('.rupiah').text().trim();
						attributes.mak = divs.eq(13).clone().children().remove().end().text().trim();
						attributes.metode_pemilihan_penyedia = divs.eq(14).clone().children().remove().end().text().trim();
						attributes.tanggal_awal_pengadaan = divs.eq(15).clone().children().remove().end().text().trim();
						attributes.tanggal_akhir_pengadaan = divs.eq(16).clone().children().remove().end().text().trim();
						attributes.tanggal_awal_pekerjaan = divs.eq(17).clone().children().remove().end().text().trim();
						attributes.tanggal_akhir_pekerjaan = divs.eq(18).clone().children().remove().end().text().trim();
						attributes.lokasi = divs.eq(19).clone().children().remove().end().text().trim();
						return attributes;
					});
	
					detilPage.open(sipengURL, 'post', JSON.stringify(detil), { "Content-Type": "application/json" }, function(status){
						console.log('OK!');
					});

					console.log(pageIndex + 1 + ' link crawled!');
					pageIndex++;
				}
			}
		}
	});
}