var _ = require('underscore'),
	webpage = require('webpage'),
	swakelolaURL = "https://sirup.lkpp.go.id/sirup/datatablectr/datarupswakelolasatker?tahun={{tahun}}&idSatker={{kode_satker}}&iDisplayLength=1000",
	swakelolaDetilURL = "https://sirup.lkpp.go.id/sirup/rup/detailPaketSwakelola/{{paket_id}}",
	sipengURL = "http://sipeng.esdm.dev/paket/penyedia",
	deleteURL = "http://sipeng.esdm.dev/paket/delete",
	system = require('system'),
	target = {}, satker, namaKLDI, namaSatker, tahun, result, onProgress = false, exclude_id = [];

var page = webpage.create(),
	detilPage = webpage.create(),
	callbackPage = webpage.create();

if(system.args.length < 2) {
	console.log('Invalid requests!');
	phantom.exit();
} else {
	satker = system.args[1];
	tahun  = system.args[2] || new Date().getFullYear();
	namaKLDI = system.args[3] || '';
	namaSatker = system.args[4] || '';
	target.swakelola = swakelolaURL.replace('{{kode_satker}}',satker).replace('{{tahun}}',tahun);
	console.log(target.swakelola);
	page.open(target.swakelola, function(status) {
		result = JSON.parse(page.plainText).aaData;
		if(status !== 'success') {
			console.log('Unable access network!');
		} else {
			var pageIndex = 0;

			var indexInterval = setInterval(function(){
				if(!onProgress && pageIndex < result.length) {
					var detilUrl = swakelolaDetilURL.replace('{{paket_id}}', result[pageIndex][0]);
					page.open(detilUrl);
				}

				if(pageIndex == result.length && exclude_id.length == result.length) {
					callbackPage.open(deleteURL, 'post', JSON.stringify({ids: exclude_id, kode_satker: satker, tipe: "SWAKELOLA", tahun_anggaran: tahun}), { "Content-Type": "application/json" }, function(status){
						console.log(status);
						phantom.exit();
					});
				}
			}, 3000);

			page.onLoadStarted = function() {
			    onProgress = true;
			};
			page.onLoadFinished = function(status){
				onProgress = false;
				// page.injectJs('https://code.jquery.com/jquery-1.11.3.min.js');
				if(status !== 'success') {
					console.log('Unable access network!');
				} else {
					var detil = page.evaluate(function(){
						var attributes = {},
							lists = $('.form-wrap').find('div').slice(1);

						attributes.paket_id = location.pathname.split('/')[4];
						attributes.tahun_anggaran = lists.eq(0).clone().children().remove().end().text().trim();
						attributes.kegiatan = lists.eq(1).clone().children().remove().end().text().trim();
						attributes.lokasi = lists.eq(2).clone().children().remove().end().text().trim();
						attributes.volume = lists.eq(3).clone().children().remove().end().text().trim();
						attributes.dana = [];

						lists.find('.rupiah').each(function(){
							var temp = {};

							temp.pagu = parseInt($(this).text());

							// cari sumber data
							var counter_one = 0,
								counter_two = 0,
								x = $(this).parent().prev(),
								y = $(this).parent().next();

							while(counter_one < 10) {

								if(x.find('strong').length) {
									temp.sumber_dana = x.clone().children().remove().end().text().trim();
									break;
								} else {
									x = x.prev();
									counter_one++;
								}
							}

							while(counter_two < 10) {

								if(y.find('strong').length) {
									temp.mak = y.clone().children().remove().end().text().trim();
									break;
								} else {
									y = y.next();
									counter_one++;
								}
							}
							attributes.dana.push(temp);
						});
						
						attributes.tanggal_awal_pekerjaan = lists.eq(lists.length - 3).clone().children().remove().end().text().trim();
						attributes.tanggal_akhir_pekerjaan = lists.eq(lists.length - 2).clone().children().remove().end().text().trim();
						attributes.deskripsi = lists.eq(lists.length - 1).clone().children().remove().end().text().trim();
						attributes.tipe = 'SWAKELOLA';
						return attributes;
					});

					exclude_id.push(detil.paket_id);
	
					detilPage.open(sipengURL, 'post', JSON.stringify(_.extend(detil, { kode_satker: satker, kldi: namaKLDI, satuan_kerja: namaSatker })), { "Content-Type": "application/json" }, function(status){
						console.log('OK!');
					});

					console.log(pageIndex + 1 + ' link crawled!');
					pageIndex++;
				}
			}
		}
	});
}