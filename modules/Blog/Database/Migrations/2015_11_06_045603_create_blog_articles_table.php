<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogArticlesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_articles', function(Blueprint $table)
        {
            $table->increments('id');
			$table->string('title');
			$table->string('permalink')->unique();
			$table->text('content')->nullable();
			$table->boolean('published')->default(false);
			$table->json('metas');
			$table->integer('category_id')->unsigned();
            $table->integer('author_id')->unsigned();
            $table->timestamps();
            $table->foreign('category_id')->references('id')->on('blog_categories')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('author_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blog_articles');
    }

}
