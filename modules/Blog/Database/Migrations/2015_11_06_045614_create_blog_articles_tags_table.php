<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogArticlesTagsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_articles_tags', function(Blueprint $table)
        {
			$table->integer('article_id')->unsigned();
			$table->integer('tag_id')->unsigned();
            $table->timestamps();
            $table->primary(['article_id', 'tag_id']);
            $table->foreign('article_id')->references('id')->on('blog_articles')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('tag_id')->references('id')->on('blog_tags')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blog_articles_tags');
    }

}
