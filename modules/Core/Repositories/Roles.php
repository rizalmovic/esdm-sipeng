<?php

namespace Modules\Core\Repositories;

use Modules\Core\Models\Roles as Model;

class Roles
{
	public static function getRoleById($id)
	{
		return Model::where(['id' => $id])->first();
	}

	public static function getStructuralRoles()
	{
		return Model::where('type','structural')->paginate();
	}

	public static function insertStructuralRole($params)
	{
		return Model::create($params);
	}

	public static function update($id, $data)
	{
		return Model::where('id',$id)->first()->update($data);
	}

	public static function delete($id)
	{
		return Model::destroy($id);
	}
}