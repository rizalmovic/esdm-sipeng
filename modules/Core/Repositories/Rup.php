<?php

namespace Modules\Core\Repositories;

use Modules\Core\Models\Rup as Model;

class Rup
{
	protected $dates = ['created_at', 'updated_at', 'tanggal_pengumuman', 'tanggal_awal_pengadaan', 'tanggal_akhir_pengadaan', 'tanggal_awal_pekerjaan', 'tanggal_akhir_pekerjaan'];
}