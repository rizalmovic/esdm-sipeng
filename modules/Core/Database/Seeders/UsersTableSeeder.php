<?php namespace Modules\Core\Database\Seeders;

use Sentinel;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = \Faker\Factory::create();

		$roles = [
			[
				'slug' => 'admin',
				'name' => 'Administrator',
				'permissions' => ['admin' => true],
				'eselon' => 'NONE',
				'type' => 'system',
				'kode_mak' => null
			],
			[
				'slug' => 'user',
				'name' => 'User',
				'permissions' => [],
				'eselon' => 'NONE',
				'type' => 'system',
				'kode_mak' => null
			]
		];

		$users = [
			[
				'email' => 'admin@example.net',
				'role'  => 'admin'
			],
			[
				'email' => 'user@example.net',
				'role'  => 'user'
			]
		];

		foreach ($roles as $role)
		{
			try
			{
				$r = Sentinel::getRoleRepository()->createModel()->create($role);
			}
			catch (Exception $e) { }
		}

		foreach ($users as $u)
		{
			try
			{
				$r = Sentinel::registerAndActivate([
					'name' 	=> $faker->name,
					'email' => $u['email'],
					'password' => 'abc123',
					'address' => $faker->address,
					'phone' => '',
				]);

				$role = Sentinel::findRoleBySlug($u['role']);

				$role->users()->attach($r);
			}
			catch (Exception $e) { }
		}
	}
}