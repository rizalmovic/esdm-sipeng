<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLpseTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lpse', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('kode_lelang')->unique();
            $table->string('nama_lelang')->nullable();
            $table->string('keterangan')->nullable();
            $table->string('tahap_lelang')->nullable();
            $table->string('agensi')->nullable();
            $table->string('satuan_kerja')->nullable();
            $table->string('kategori')->nullable();
            $table->string('metode_pengadaan')->nullable();
            $table->string('metode_dokumen')->nullable();
            $table->string('metode_kualifikasi')->nullable();
            $table->string('metode_evaluasi')->nullable();
            $table->integer('tahun_anggaran')->nullable();
            $table->string('sumber_dana')->nullable();
            $table->decimal('pagu')->nullable();
            $table->decimal('hps')->nullable();
            $table->string('cara_pembayaran')->nullable();
            $table->string('pembebanan_tahun_anggaran')->nullable();
            $table->string('sumber_pendanaan')->nullable();
            $table->string('lokasi_pekerjaan')->nullable();
            $table->text('syarat_kualifikasi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lpse');
    }

}
