<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRupTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rup', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('kldi')->nullable();
            $table->string('satuan_kerja')->nullable();
            $table->integer('tahun_anggaran')->nullable();
            $table->string('kegiatan')->nullable();
            $table->string('paket_nama')->nullable();
            $table->string('paket_id')->unique();
            $table->string('jenis_belanja')->nullable();
            $table->string('volume')->nullable();
            $table->string('deskripsi')->nullable();
            $table->date('tanggal_pengumuman')->nullable();
            $table->string('sumber_dana')->nullable();
            $table->decimal('pagu')->default(0);
            $table->string('mak')->nullable();
            $table->string('metode_pemilihan_penyedia')->nullable();
            $table->date('tanggal_awal_pengadaan')->nullable();
            $table->date('tanggal_akhir_pengadaan')->nullable();
            $table->date('tanggal_awal_pekerjaan')->nullable();
            $table->date('tanggal_akhir_pekerjaan')->nullable();
            $table->string('lokasi')->nullable();
            $table->string('kode_lelang')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rup');
    }

}
