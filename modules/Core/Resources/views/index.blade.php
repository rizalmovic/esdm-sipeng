@extends('layouts.main')

@section('content')

<div class="row">
	<div class="col-md-4">
		<div class="panel panel-default">
			<form method="GET">
			<div class="panel-body">
				<div class="form-group">
				<label>Tahun Anggaran</label>
					<select name="tahun" id="tahun" class="form-control">
						<option value="">Pilih Tahun Anggaran</option>
						@foreach(\Modules\Core\Models\Rup::distinct('tahun_anggaran')->get() as $data)
							<option value="{{$data[0]}}" {{($data[0] == Input::get('tahun', null)) ? 'selected' : ''}}>{{$data[0]}}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="panel-footer">
				<button class="btn btn-sm btn-primary">Filter</button>
			</div>
			</form>
		</div>
		<div class="panel panel-default">
			<div class="panel-body">
				<canvas id="pieLelangChart" style="width:100%; height: 200px"></canvas>
			</div>
			<div class="panel-footer">
				<p align="center"><strong>Grafik Perbandingan paket telah lelang dan belum lelang seluruh satuan kerja</strong></p>
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-title">Paket Strategis</div>
			</div>
			<table class="table">
				<thead>
					<th>Satuan Kerja</th>
					<th>Paket Lelang</th>
					<th>Paket belum lelang</th>
					<th>Jumlah Paket</th>
				</thead>
				<tbody>
					@foreach($penyedia['retval'] as $d)
						<tr>
							<td>{{$d['satuan_kerja']}}</td>
							<td>{{$d['lelang']}}</td>
							<td>{{$d['belum_lelang']}}</td>
							<td>{{$d['lelang'] + $d['belum_lelang']}}</td>
						</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<th>Total</th>
						<td>{{array_sum(array_pluck($penyedia['retval'],'lelang'))}}</td>
						<td>{{array_sum(array_pluck($penyedia['retval'],'belum_lelang'))}}</td>
						<td>{{array_sum(array_pluck($penyedia['retval'],'lelang')) + array_sum(array_pluck($penyedia['retval'],'belum_lelang'))}}</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>

@endsection

@section('scripts')
<script src="{{asset('js/Chart.min.js')}}"></script>
<script>
var pieLelang = {!! json_encode($lelang['pie_lelang']) !!};
var pieLelangChartEl = document.getElementById('pieLelangChart');
var pieLelangChart = new Chart(pieLelangChartEl.getContext('2d')).Pie(pieLelang, { responsive: true});
</script>
@endsection