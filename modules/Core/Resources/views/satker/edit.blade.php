@extends('layouts.main')

@section('content')
	<ol class="breadcrumb">
		<li><a href="javascript:;">Pengaturan</a></li>
		<li><a href="{{route('satker.index')}}">{{$params['title']}}</a></li>
		<li class="active">Buat {{$params['title']}} baru</li>
	</ol>

	<div class="panel panel-default">
		<div class="panel-heading clearfix">
			<span class="panel-title">{{$params['title']}}</span>
		</div>
		{!! Former::populate($role) !!}
		{!! Former::open(route('satker.update', $role->id))->method('put') !!}
		<div class="panel-body">
			{!! Former::text('name')->label('Nama')->placeholder('Nama Satuan Kerja')->required() !!}
			{!! Former::select('eselon')
					->options(['NONE' => 'Bukan Eselon', '1' => 'Eselon I','2' => 'Eselon II',3 => 'Eselon III', 4 => 'Eselon IV'])
					->data_selectize(true)
					->required() !!}
			{!! Former::text('kode_mak')->placeholder('Kode MAK')
					->data_selectize(true)
					->data_options('{ "delimiter":",", "persist": false, "create": true }') !!}
		</div>
		<div class="panel-footer clearfix">
			<div class="pull-right">
				<button class="btn btn-sm btn-primary">Simpan</button>
				<a href="{{route('satker.index')}}" class="btn btn-sm btn-default">Batal</a>
			</div>
		</div>
		{!! Former::close() !!}
	</div>
@endsection