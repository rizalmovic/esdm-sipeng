@extends('layouts.main')

@section('content')

	<ol class="breadcrumb">
		<li><a href="javascript:;">Pengaturan</a></li>
		<li class="active">{{$params['title']}}</li>
	</ol>

	<div class="panel panel-default">
		<div class="panel-heading">
			<span class="panel-title">{{$params['title']}}</span>
		</div>
		@if($roles->count())
			<table class="table">
				<thead>
					<tr>
						<th>Nama</th>
						<th>Eselon</th>
						<th>Kegiatan</th>
						<th width="150px"></th>
					</tr>
				</thead>
				<tbody>
					@foreach($roles as $role)
						<tr>
							<td>{{$role->name}}</td>
							<td>{{$role->eselon}}</td>
							@if(!$role->kode_mak)
								<td>&dash;</td>
							@else
								<td>
									@foreach($role->kode_mak as $mak)
										<li>{{$mak}}</li>
									@endforeach
								</td>
							@endif
							<td>
								{!! Former::open(route('satker.delete', $role->id))->method('delete') !!}
									<a class="btn btn-sm btn-default" href="{{route('satker.edit', $role->id)}}">Ubah</a>
									<button class="btn btn-sm btn-default">Hapus</button>
								{!! Former::close() !!}
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			<div class="panel-footer clearfix">
				<div class="pull-left">
					<a href="{{route('satker.create')}}" class="btn btn-sm btn-success"><i class="fa fa-fw fa-plus"></i> Buat {{$params['title']}}</a>
				</div>
				<div class="pull-right">
					{!! $roles->render() !!}
				</div>
			</div>
		@else
			<div class="panel-body">
				<p><i class="fa fa-fw fa-info"></i> Data {{$params['title']}} masih kosong.</p>
			</div>
			<div class="panel-footer">
				<div class="pull-left">
				<a href="{{route('satker.create')}}" class="btn btn-sm btn-success"><i class="fa fa-fw fa-plus"></i> Buat {{$params['title']}}</a>
			</div>
			</div>
		@endif
	</div>

@endsection