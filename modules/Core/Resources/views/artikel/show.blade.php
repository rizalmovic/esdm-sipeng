@extends('layouts.main')

@section('content')
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">{{$data->title}}</h3>
		</div>
		<div class="panel-body">
			{!! $data->content !!}
		</div>
		<div class="panel-footer clearfix">
			<div class="pull-right">
				<a class="btn btn-default" href="javascript:;" onclick="window.history.back()">Kembali</a>
			</div>
		</div>
	</div>
@endsection