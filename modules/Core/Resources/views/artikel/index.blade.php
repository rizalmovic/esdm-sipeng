@extends('layouts.main')

@section('content')
	<div class="panel panel-default">
		<div class="panel-heading">
			
			@if(Sentinel::getUser() && Sentinel::getUser()->hasAccess('admin'))
				<div class="pull-right">
					<a href="{{route('artikel.create')}}" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-plus"></i> Artikel Baru</a>
				</div>
			@endif

			<div class="panel-title">Artikel</div>
		</div>
		<table class="table">
			<thead>
				<tr>
					<th>Judul</th>
					<th>Tanggal Terbit</th>
					@if(Sentinel::getUser() && Sentinel::getUser()->hasAccess('admin'))	
						<th></th>
					@endif
				</tr>
			</thead>
			<tbody>
				@foreach($data as $d)
					<tr>
						<td>
							<a href="{{route('artikel.view', $d->id)}}">{{$d->title}}</a>
						</td>
						<td>{{$d->created_at->format('d-m-Y')}}</td>
						@if(Sentinel::getUser() && Sentinel::getUser()->hasAccess('admin'))	
							<td align="right">
								<a href="{{route('artikel.edit', $d->id)}}" class="btn btn-default"><i class="fa fa-fw fa-pencil"></i></a>
								<a href="{{route('artikel.delete', $d->id)}}" class="btn btn-danger"><i class="fa fa-fw fa-times"></i></a>
							</td>
						@endif
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection