@extends('layouts.main')

@section('content')
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">{{$params['title'] or 'Kelola Artikel'}}</h3>
		</div>
		{!! \Former::populate($data) !!}
		{!! \Former::open(route('artikel.update', $data->id))->class('form-vertical')->method('put') !!}
		<div class="panel-body">
			<div class="form-group">
				<label for="judul">Judul</label>
				<input id="judul" name="judul" type="text" class="form-control" placeholder="Judul" value="{{$data->judul}}">
			</div>
			<div class="form-group">
				<label for="permalink">Permalink</label>
				<input id="permalink" name="permalink" type="text" class="form-control" placeholder="Permalink" value="{{$data->permalink}}">
			</div>
			<div class="form-group">
				<label for="deskripsi">Deskripsi</label>
				<textarea name="deskripsi" id="deskripsi" rows="4" class="form-control">{{$data->deskripsi}}</textarea>
			</div>
			<div class="form-group">
				<label for="isi">Isi</label>
				<textarea name="isi" id="isi" class="form-control">{{$data->isi}}</textarea>
			</div>
		</div>
		<div class="panel-footer">
			<button class="btn btn-primary">Simpan</button>
			<a href="{{route('artikel.index')}}" class="btn btn-default">Kembali</a>
		</div>
		{!! \Former::close() !!}
	</div>
@endsection


@section('scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.4/ckeditor.js"></script>
<script>
$(function(){
	var page = {
		events: function(){
			$('#judul').on('focusout', function(){
				var $judul = $(this),
					$permalink = $('#permalink');

				if($judul.val().length && !$permalink.val().length) {
					var fomattedString = encodeURI($judul.val().replace(/\$|\?|\&|\=|\%|\*|\^|\#|\@|\:|\;|\,/g,'').replace(/ /g,'-').toLowerCase());
					$permalink.val(fomattedString);
				}
			});
		},
		init: function(){
			CKEDITOR.replace('isi');
			this.events();
		}
	};
	page.init();
});
</script>
@endsection