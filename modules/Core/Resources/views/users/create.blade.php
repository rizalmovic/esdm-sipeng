@extends('layouts.main')

@section('content')

	<ol class="breadcrumb">
		<li><a href="javascript:;">Pengaturan</a></li>
		<li><a href="{{route('pengguna.index')}}">{{$params['title']}}</a></li>
		<li class="active">Buat {{$params['title']}} baru</li>
	</ol>

	<div class="panel panel-default">
		<div class="panel-heading clearfix">
			<span class="panel-title">{{$params['title']}}</span>
		</div>
		{!! Former::open(route('pengguna.postCreate')) !!}
		<div class="panel-body">
			@include('core::users._form')
		</div>
		<div class="panel-footer clearfix">
			<div class="pull-right">
				<button class="btn btn-sm btn-primary">Simpan</button>
				<a href="{{route('pengguna.index')}}" class="btn btn-sm btn-default">Batal</a>
			</div>
		</div>
		{!! Former::close() !!}
	</div>
@endsection