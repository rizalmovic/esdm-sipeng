@extends('layouts.main')

@section('content')

	<ol class="breadcrumb">
		<li><a href="javascript:;">Pengaturan</a></li>
		<li class="active">{{$params['title']}}</li>
	</ol>

	<div class="panel panel-default">
		<div class="panel-heading">
			<span class="panel-title">{{$params['title']}}</span>
		</div>
		@if($users->count())
			<table class="table">
				<thead>
					<tr>
						<th>Nama</th>
						<th>Email</th>
						<th>Peran</th>
						<th width="230px"></th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $user)
						<tr>
							<td>{{$user->name}}</td>
							<td>{{$user->email}}</td>
							@if(!$user->roles)
								<td>&dash;</td>
							@else
								<td>
									@foreach($user->roles as $role)
										<li>{{$role->name}}</li>
									@endforeach
								</td>
							@endif
							<td>
								{!! Former::open(route('pengguna.delete', $user->id))->method('delete') !!}
									<a href="{{route('pengguna.reset', $user->email)}}" class="btn btn-sm btn-default">Reset Password</a>
									<a class="btn btn-sm btn-default" href="{{route('pengguna.edit', $user->id)}}">Ubah</a>
									<button class="btn btn-sm btn-default">Hapus</button>
								{!! Former::close() !!}
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			<div class="panel-footer clearfix">
				<div class="pull-left">
					<a href="{{route('pengguna.create')}}" class="btn btn-sm btn-success"><i class="fa fa-fw fa-plus"></i> Buat {{$params['title']}}</a>
				</div>
				<div class="pull-right">
					{!! $users->render() !!}
				</div>
			</div>
		@else
			<div class="panel-body">
				<p><i class="fa fa-fw fa-info"></i> Data {{$params['title']}} masih kosong.</p>
			</div>
			<div class="panel-footer">
				<div class="pull-left">
				<a href="{{route('satker.create')}}" class="btn btn-sm btn-success"><i class="fa fa-fw fa-plus"></i> Buat {{$params['title']}}</a>
			</div>
			</div>
		@endif
	</div>

@endsection