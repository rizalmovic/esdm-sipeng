{!! Former::text('name')->label('Nama')->placeholder('Nama')->required() !!}
{!! Former::text('email')->label('Email')->placeholder('Email')->required() !!}
{!! Former::select('roles[]')->label('Roles')
	->fromQuery(\Modules\Core\Models\Roles::get(['id','name']),'name','id')
	->data_selectize(true)
	->multiple() !!}

