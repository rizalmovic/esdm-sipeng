@extends('layouts.main')

@section('content')

	<div class="panel panel-default">
		<div class="panel-heading clearfix">
			<span class="panel-title"><i class="fa fa-fw fa-male"></i> My Profile</span>
		</div>
		{!! Former::populate($user) !!}
		{!! Former::populateField('roles[]', $user->roles) !!}
		{!! Former::open(route('update_profile', $user->id))->method('post') !!}
		<div class="panel-body">
			{!! Former::text('name')->label('Nama')->placeholder('Nama')->required() !!}
			{!! Former::text('email')->label('Email')->placeholder('Email')->required() !!}
		</div>
		<div class="panel-footer clearfix">
			<div class="pull-right">
				<button class="btn btn-sm btn-primary">Simpan</button>
			</div>
		</div>
		{!! Former::close() !!}
	</div>

	<div class="panel panel-default">
		<div class="panel-heading clearfix">
			<span class="panel-title"><i class="fa fa-fw fa-key"></i> Change Password</span>
		</div>
		{!! Former::populate($user) !!}
		{!! Former::populateField('roles[]', $user->roles) !!}
		{!! Former::open(route('update_profile', $user->id))->method('post') !!}
		<div class="panel-body">
			{!! Former::hidden('type')->value('password')!!}
			{!! Former::password('old_password')->label('Password Lama')->placeholder('Password Lama')->required() !!}
			{!! Former::password('password')->label('Password Baru')->placeholder('Password Baru')->required() !!}
			{!! Former::password('password_confirmation')->label('Konfirmasi')->placeholder('Konfirmasi Password')->required() !!}
		</div>
		<div class="panel-footer clearfix">
			<div class="pull-right">
				<button class="btn btn-sm btn-primary">Simpan</button>
			</div>
		</div>
		{!! Former::close() !!}
	</div>

@endsection