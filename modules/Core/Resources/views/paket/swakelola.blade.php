@extends('layouts.main')

<?php
$filters['tahun_anggaran'] = Input::get('tahun_anggaran', false);
$filters['satuan_kerja'] = Input::get('satuan_kerja', false);
$total_pagu = 0;
?>

@section('content')

	<ol class="breadcrumb">
		<li><a href="javascript:;">Rencana Umum Pengadaan</a></li>
		<li class="active">{{$params['title']}}</li>
	</ol>

	<div class="panel panel-default">
		<div class="panel-heading">
			<span class="panel-title">{{$params['title']}}</span>
		</div>
		@if($rup->count())
			<div class="table-responsive">
				<table class="table table-bordered table-detail">
					<thead>
						<tr>
							<th rowspan="2" colspan="2" width="40%" class="text-center text-middle">Nama Kegiatan</th>
							<th rowspan="2" width="15%" class="text-center text-middle">Pagu (Rp.)</th>
							<th colspan="2" class="text-center text-middle">Tanggal</th>
							<th rowspan="2" class="text-center text-middle">PIC</th>
						</tr>
						<tr>
							<th class="text-center">Awal Pekerjaan</th>
							<th class="text-center">Akhir Pekerjaan</th>
						</tr>
					</thead>
					<tbody>
						@foreach($rup as $r)
							<?php $total_pagu += array_sum(array_pluck($r->dana,'pagu'));  ?>
							<tr>
								<td class="text-center text-middle">
									<a class="show-detail-btn" href="javascript:;"><i class="fa fa-fw fa-caret-right fa-caret-down"></i></a>
								</td>
								<td>{{$r->kegiatan}}</td>
								<td class="text-middle text-right">{{ number_format(array_sum(array_pluck($r->dana,'pagu')),0,'','.') }}</td>
								<td class="text-middle text-center">{{$r->tanggal_awal_pekerjaan->format('d/m/Y')}}</td>
								<td class="text-middle text-center">{{$r->tanggal_akhir_pekerjaan->format('d/m/Y')}}</td>
								<td class="text-middle text-center">
									@if(Sentinel::check())
										<a href="javascript:;" class="edit" data-emptytext='Update' data-name="pic" data-type="select" data-pk="{{$r->id}}" data-source="{{route('api.users')}}" data-url="{{route('postSingleColumnUpdate')}}">{{$r->pic}}</a>
									@else
										{{ $r->pic or '-' }}
									@endif
								</td>
							</tr>
							<tr class="detil hide">
								<td colspan="8" class="bg-custom-gray">
									<table class="table table-bordered table-condensed" style="margin:0">
										<tr class="bg-primary">
											<th colspan="2">Detil</th>
										</tr>
										<tr>
											<td width="20%">K/L/D/I</td>
											<td>{{$r->kldi}}</td>
										</tr>
										<tr>
											<td>Satuan Kerja</td>
											<td>{{$r->satuan_kerja}}</td>
										</tr>
										<tr>
											<td>Tahun Anggaran</td>
											<td>{{$r->tahun_anggaran}}</td>
										</tr>
										<tr>
											<td>Volume</td>
											<td>{{$r->volume}}</td>
										</tr>
										<tr>
											<td>Deskripsi</td>
											<td>{{$r->deskripsi}}</td>
										</tr>
										<?php $i = 1; ?>
										@foreach($r->dana as $dana)
										<tr>
											<td>Sumber Dana ({{$i}})</td>
											<td>{{$dana['sumber_dana']}}</td>
										</tr>
										<tr>
											<td>Pagu ({{$i}})</td>
											<td>{{number_format($dana['pagu'],0,'','.')}}</td>
										</tr>
										<tr>
											<td>Kode MAK ({{$i}})</td>
											<td>{{$dana['mak']}}</td>
										</tr>
										<?php $i++; ?>
										@endforeach
									</table>
								</td>
							</tr>
						@endforeach
					</tbody>
					<tfoot>
						<tr>
							<th colspan="2" class="text-center text-middle">Total ({{$rup->count()}} paket)</th>
							<td class="text-right text-middle">
								<div class="pull-left clearfix">Rp.</div>
								{{ number_format($total_pagu,0,'','.') }}
							</td>
							<td colspan="3" class="text-center text-middle">-</td>
						</tr>
					</tfoot>
				</table>
			</div>
		@else
			<div class="panel-body">
				Record RUP Swakelola tidak ditemukan
			</div>
		@endif
	</div>

@endsection

@section('styles')
<link rel="stylesheet" href="{{asset('css/custom-theme/jquery-ui-1.9.2.custom.css')}}">
<link rel="stylesheet" href="{{asset('css/jqueryui-editable.css')}}">
@endsection

@section('scripts')
<script src="{{asset('js/jquery-ui-1.9.2.custom.min.js')}}"></script>
<script src="{{asset('js/jqueryui-editable.min.js')}}"></script>
<script src="{{asset('js/lelang.js')}}"></script>
<script src="{{asset('js/editable.js')}}"></script>
@endsection