@extends('layouts.main')

<?php
$filters['tahun_anggaran'] = Input::get('tahun_anggaran', false);
$filters['satuan_kerja'] = Input::get('satuan_kerja', false);
$filters['metode_pemilihan_penyedia'] = Input::get('metode_pemilihan_penyedia', false);
?>

@section('content')
	<ol class="breadcrumb">
		<li><a href="javascript:;">Realisasi</a></li>
		<li class="active">{{$params['title']}}</li>
	</ol>

	<div class="panel panel-default">
		<div class="panel-heading">
			<span class="panel-title">{{$params['title']}}</span>
		</div>
		@if($rup->count())
			<div class="table-responsive">
				<table class="table table-bordered table-detail">
					<thead>
						<tr>
							<th rowspan="2" colspan="2" width="25%" class="text-center text-middle">Nama Paket</th>
							<th rowspan="2" width="12%" class="text-center text-middle">Pagu</th>
							<th rowspan="2" width="12%" class="text-center text-middle">HPS</th>
							<th colspan="3" class="text-center text-middle">Tanggal</th>
							<th colspan="4" class="text-center text-middle">Kontrak</th>
							<th rowspan="2" class="text-center text-middle">PIC</th>
						</tr>
						<tr>
							<th class="text-center">Pengumuman</th>
							<th class="text-center">Awal Pengadaan</th>
							<th class="text-center">Akhir Pengadaan</th>
							<th class="text-center">No</th>
							<th class="text-center">Nilai</th>
							<th class="text-center">Mulai</th>
							<th class="text-center">Akhir</th>
						</tr>
					</thead>
					<tbody>
						@foreach($rup as $r)
							<tr>
								<td class="text-right text-middle {{ ($r->status_pekerjaan == 'selesai') ? 'bg-primary':'bg-custom-gray'}}" width="10">
									<a class="show-detail-btn" href="javascript:;"><i class="fa fa-fw fa-caret-right fa-caret-down"></i></a>
								</td>
								<td >{{$r->paket_nama}}</td>
								<td class="text-right text-middle">
									@if($r->lpse)
									<div class="pull-left clearfix">Rp.</div>
									{{ number_format($r->lpse['pagu'],0,'','.') }}
									@else
									<div class="pull-left clearfix">Rp.</div>
									{{ number_format(array_sum(array_pluck($r->dana,'pagu')),0,'','.') }}
									@endif
								</td>
								<td class="text-right text-middle">
									@if($r->lpse)
									<div class="pull-left clearfix">Rp.</div>
									{{ number_format($r->lpse['hps'],0,'','.') }}
									@else
									-
									@endif
								</td>
								<td class="text-center text-middle">
									@if($r->lpse)
									{{date('d/m/Y',$r->lpse['jadwal']['pengumuman_pascakualifikasi']['mulai']->sec)}}
									@else
									{{date('d/m/Y',strtotime($r->tanggal_pengumuman))}}
									@endif
								</td>
								@if($r->metode_pemilihan_penyedia != 'e-Purchasing' && $r->lpse)
									<td class="text-center text-middle">{{ date('d/m/Y', $r->lpse['jadwal']['download_dokumen_pengadaan']['mulai']->sec)}}</td>
									<td class="text-center text-middle">{{ date('d/m/Y', $r->lpse['jadwal']['download_dokumen_pengadaan']['sampai']->sec)}}</td>
								@else
									<td class="text-center text-middle">
										{{date('d/m/Y', strtotime($r->tanggal_awal_pengadaan) )}}
									</td>
									<td class="text-center text-middle">
										{{date('d/m/Y',strtotime($r->tanggal_akhir_pengadaan))}}
									</td>
								@endif
								
								<td class="text-center text-middle">
									@if(Sentinel::check())
										<a href="javascript:;" class="edit" data-emptytext='Update' data-name="no_kontrak" data-type="text" data-pk="{{$r->id}}" data-url="{{route('postSingleColumnUpdate')}}">{{$r->no_kontrak}}</a>
									@else
										{{$r->no_kontrak or '-'}}
									@endif
								</td>
								<td class="text-right text-middle">
									@if(Sentinel::check())
										<div class="pull-left clearfix">Rp.</div>
										<a href="javascript:;" class="edit" data-emptytext='Update' data-name="nilai_kontrak" data-type="text" data-pk="{{$r->id}}" data-url="{{route('postSingleColumnUpdate')}}">{{number_format($r->nilai_kontrak,0,'','.')}}</a>
									@else
										{{$r->nilai_kontrak or '-'}}
									@endif
								</td>
								<td class="text-center text-middle">
									@if(Sentinel::check())
										<a href="javascript:;" class="edit" data-emptytext='Update' data-name="tanggal_awal_kontrak" data-type="date" data-format="yyyy-mm-dd" data-viewFormat="dd/mm/yyyy" data-pk="{{$r->id}}" data-url="{{route('postSingleColumnUpdate')}}">{{($r->tanggal_awal_kontrak) ? date('d/m/Y', $r->tanggal_awal_kontrak->sec) : ''}}</a>
									@else
										{{($r->tanggal_awal_kontrak) ? date('d/m/Y', $r->tanggal_awal_kontrak->sec) : '-'}}
									@endif
								</td>
								<td class="text-center text-middle">
									@if(Sentinel::check())
										<a href="javascript:;" class="edit" data-emptytext='Update' data-name="tanggal_akhir_kontrak" data-type="date" data-format="yyyy-mm-dd" data-viewFormat="dd/mm/yyyy" data-pk="{{$r->id}}" data-url="{{route('postSingleColumnUpdate')}}">{{ ($r->tanggal_akhir_kontrak) ? date('d/m/Y', $r->tanggal_akhir_kontrak->sec) : ''}}</a>
									@else
										{{ ($r->tanggal_akhir_kontrak) ? date('d/m/Y', $r->tanggal_akhir_kontrak->sec) : '-'}}
									@endif
								</td>
								<td class="text-center text-middle">
									{{ $r->pic or '-' }}
								</td>
							</tr>
							<tr class="detil hide">
								<td colspan="12" class="bg-custom-gray">
									@if($r->lpse)
									<table class="table table-bordered table-condensed" style="margin:0">
										<tr class="bg-primary">
											<th colspan="2">Detil</th>
										</tr>
										<tr>
											<th>Nama Paket Lelang</th>
											<td>{{$r->lpse['paket_nama']}}</td>
										</tr>
										<tr>
											<th>Keterangan</th>
											<td>{{$r->lpse['keterangan']}}</td>
										</tr>
										<tr>
											<th>Tahap</th>
											<td>{{$r->lpse['tahap']}}</td>
										</tr>
										<tr>
											<th>Kategori</th>
											<td>{{$r->lpse['kategori']}}</td>
										</tr>
										<tr>
											<th>Metode Pemilihan</th>
											<td>{{$r->lpse['metode_pemilihan']}}</td>
										</tr>
										<tr>
											<th>Metode Dokumen</th>
											<td>{{$r->lpse['metode_dokumen']}}</td>
										</tr>
										<tr>
											<th>Metode Kualifikasi</th>
											<td>{{$r->lpse['metode_kualifikasi']}}</td>
										</tr>
										<tr>
											<th>Metode Evaluasi</th>
											<td>{{$r->lpse['metode_evaluasi']}}</td>
										</tr>
										<tr>
											<th>Anggaran</th>
											<td>{{$r->lpse['anggaran']}}</td>
										</tr>
										<tr>
											<th>Cara Pembayaran</th>
											<td>{{$r->lpse['cara_pembayaran']}}</td>
										</tr>
										<tr>
											<th>Pembebanan Tahun Anggaran</th>
											<td>{{$r->lpse['pembebanan_tahun_anggaran']}}</td>
										</tr>
										<tr>
											<th>Sumber Pendanaan</th>
											<td>{{$r->lpse['sumber_pendanaan']}}</td>
										</tr>
										<tr>
											<th>Kualifikasi Usaha</th>
											<td>{{$r->lpse['kualifikasi_usaha']}}</td>
										</tr>
										<tr>
											<th>Lokasi Pekerjaan</th>
											<td>{{$r->lpse['lokasi_pekerjaan']}}</td>
										</tr>
										<tr class="text-middle">
											<th>LPSE</th>
											<td>
												<a target="_blank" href="http://eproc.esdm.go.id/eproc/lelang/view/{{$r->kode_lelang}}" class="btn btn-sm btn-default">Informasi Lelang</a>
												<a target="_blank" href="http://eproc.esdm.go.id/eproc/lelang/tahap/{{$r->kode_lelang}}" class="btn btn-sm btn-default">Jadwal</a>
												<a target="_blank" href="http://eproc.esdm.go.id/eproc/rekanan/lelangpeserta/{{$r->kode_lelang}}" class="btn btn-sm btn-default">Peserta</a>
											</td>
										</tr>
									</table>
									@else
										Tidak ada data Lelang
									@endif
								</td>
							</tr>
						@endforeach
					</tbody>
					<tfoot>
						<tr>
							<th colspan="2" class="text-center text-middle">
								Total ({{$rup->count()}} paket)
							</th>
							<td class="text-right text-middle">
								<div class="pull-left clearfix">Rp.</div>
								{{ number_format($rup->sum('lpse.pagu'),0,'','.')}}
							</td>
							<td class="text-right text-middle">
								<div class="pull-left clearfix">Rp.</div>
								{{ number_format($rup->sum('lpse.hps'),0,'','.')}}
							</td>
							<td colspan="4" class="text-center text-middle">-</td>
							<td class="text-right text-middle">
								<div class="pull-left clearfix">Rp.</div>
								{{ number_format($rup->sum('nilai_kontrak'),0,'','.')}}
							</td>
							<td colspan="3" class="text-center text-middle">-</td>
						</tr>
					</tfoot>
				</table>
			</div>
		@else
			<div class="panel-body">
				Record RUP Penyedia tidak ditemukan
			</div>
		@endif
	</div>
@endsection

@section('styles')
<link rel="stylesheet" href="{{asset('css/custom-theme/jquery-ui-1.9.2.custom.css')}}">
<link rel="stylesheet" href="{{asset('css/jqueryui-editable.css')}}">
@endsection

@section('scripts')
<script src="{{asset('js/jquery-ui-1.9.2.custom.min.js')}}"></script>
<script src="{{asset('js/jqueryui-editable.min.js')}}"></script>
<script src="{{asset('js/lelang.js')}}"></script>
<script src="{{asset('js/editable.js')}}"></script>
@endsection