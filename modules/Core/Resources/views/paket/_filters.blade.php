<form class="form-vertical" method="get">
	<div class="form-group">
		<div class="input-group">
	      <input name="search" type="text" class="form-control" placeholder="Cari paket..." value="{{Input::get('search','')}}">
	      <span class="input-group-btn">
	        <button type="submit" class="btn btn-success"><i class="fa fa-search"></i></button>
	      </span>
	    </div><!-- /input-group -->
    </div>
	<div class="form-group">
		<label>Tahun Anggaran</label>
		<select name="tahun_anggaran" id="tahun_anggaran" class="form-control">
			<option value="">Semua</option>
			@foreach(\Modules\Core\Models\Rup::distinct('tahun_anggaran')->get() as $data)
				<option value="{{$data[0]}}" {{($data[0] == $filters['tahun_anggaran']) ? 'selected' : ''}}>{{$data[0]}}</option>
			@endforeach
		</select>
	</div>
	<div class="form-group">
		<label>Satuan Kerja</label>
		<select name="satuan_kerja" id="satuan_kerja" class="form-control">
			<option value="">Semua</option>
			@foreach(\Modules\Core\Models\Rup::distinct('satuan_kerja')->get() as $data)
				<option value="{{strtolower(str_replace(' ','_',$data[0]))}}" {{(strtolower(str_replace(' ','_',$data[0])) == $filters['satuan_kerja']) ? 'selected' : '' }}>{{ucwords(strtolower($data[0]))}}</option>
			@endforeach
		</select>
	</div>
	@if(isset($params['title']) && $params['title'] == 'Penyedia')
	<div class="form-group">
		<label>Metode Pemilihan Penyedia</label>
		<select name="metode_pemilihan_penyedia" id="metode_pemilihan_penyedia" class="form-control">
			<option value="">Semua</option>
			@foreach(\Modules\Core\Models\Rup::distinct('metode_pemilihan_penyedia')->get() as $data)
				<option value="{{strtolower(str_replace(' ','_',$data[0]))}}" {{ (strtolower(str_replace(' ','_',$data[0])) == $filters['metode_pemilihan_penyedia']) ? 'selected' : ''}}>{{ucwords(strtolower($data[0]))}}</option>
			@endforeach
		</select>
	</div>
	@endif
	<button class="btn btn-block btn-primary">Filter</button>
</form>