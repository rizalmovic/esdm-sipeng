@extends('layouts.main')

<?php
	$filters['tahun_anggaran'] = Input::get('tahun_anggaran', false);
	$filters['satuan_kerja'] = Input::get('satuan_kerja', false);
	$filters['metode_pemilihan_penyedia'] = Input::get('metode_pemilihan_penyedia', false);
?>

@section('content')
	<ol class="breadcrumb">
		<li><a href="javascript:;">Rencana</a></li>
		<li class="active">{{$params['title']}}</li>
	</ol>

	<div class="panel panel-default">
		<div class="panel-heading">
			<span class="panel-title">{{$params['title']}}</span>
		</div>
		@if($rup->count())
			<div class="table-responsive">
				<table class="table table-bordered table-detail">
					<thead>
						<tr>
							<th rowspan="2" colspan="3" width="30%" class="text-center text-middle">Nama Paket</th>
							<th rowspan="2" width="15%" class="text-center text-middle">Pagu (Rp.)</th>
							<th colspan="5" class="text-center text-middle">Tanggal</th>
							<th rowspan="2" class="text-center text-middle">Kode Lelang</th>
							<th rowspan="2" class="text-center text-middle">PIC</th>
						</tr>
						<tr>
							<th class="text-center">Pengumuman</th>
							<th class="text-center">Awal Pengadaan</th>
							<th class="text-center">Akhir Pengadaan</th>
							<th class="text-center">Awal Pekerjaan</th>
							<th class="text-center">Akhir Pekerjaan</th>
						</tr>
					</thead>
					<tbody>
						@foreach($rup as $r)
							<tr>
								<td class="text-right text-middle {{ $r->kode_lelang ? 'bg-primary' : ( strtotime('now') > strtotime($r->tanggal_awal_pengadaan)  ? 'bg-custom-light-yellow' : '' )  }}"
									width="10">
									<a class="show-detail-btn" href="javascript:;"><i class="fa fa-fw fa-caret-right fa-caret-down"></i></a>
								</td>
								<td class="text-right text-middle">
									{{$r->paket_id}}
								</td>
								<td>
									{{$r->paket_nama}}
								</td>
								<td class="text-right text-middle">
									<div class="pull-left clearfix">Rp.</div>
									{{ number_format(array_sum(array_pluck($r->dana, 'pagu')),0,'','.') }}
								</td>
								<td class="text-center text-middle">
									{{date('d/m/Y', strtotime($r->tanggal_pengumuman))}}
								</td>
								<td class="text-center text-middle">
									{{date('d/m/Y', strtotime($r->tanggal_awal_pengadaan))}}
								</td>
								<td class="text-center text-middle">
									{{date('d/m/Y', strtotime($r->tanggal_akhir_pengadaan))}}
								</td>
								<td class="text-center text-middle">
									{{date('d/m/Y', strtotime($r->tanggal_awal_pekerjaan))}}
								</td>
								<td class="text-center text-middle">
									{{date('d/m/Y', strtotime($r->tanggal_akhir_pekerjaan))}}
								</td>
								<td class="text-center text-middle">
									@if(Sentinel::check() && ($r->metode_pemilihan_penyedia != 'e-Purchasing'))
										<a href="javascript:;" class="edit" data-emptytext='Update' data-name="kode_lelang" data-type="text" data-pk="{{$r->id}}" data-url="{{route('postSingleColumnUpdate')}}">{{$r->kode_lelang}}</a>
										@if($r->kode_lelang && !$r->lpse)
											<i class="fa fa-fw fa-exclamation"></i> Kode Lelang Tidak Valid
										@endif
									@else
										{{ $r->kode_lelang or '-' }}
									@endif
								</td>
								<td class="text-center text-middle">
									@if(Sentinel::check())
										<a href="javascript:;" class="edit" data-emptytext='Update' data-name="pic" data-type="select" data-pk="{{$r->id}}" data-source="{{route('api.users')}}" data-url="{{route('postSingleColumnUpdate')}}">{{$r->pic}}</a>
									@else
										{{ $r->pic or '-' }}
									@endif
								</td>
							</tr>
							<tr class="detil hide">
								<td colspan="11" class="bg-custom-gray">
									<table class="table table-bordered table-condensed" style="margin:0">
										<tr class="bg-primary">
											<th colspan="2">Detil</th>
										</tr>
										<tr>
											<td width="20%">K/L/D/I</td>
											<td>{{$r->kldi}}</td>
										</tr>
										<tr>
											<td>Satuan Kerja</td>
											<td>{{$r->satuan_kerja}}</td>
										</tr>
										<tr>
											<td>Tahun Anggaran</td>
											<td>{{$r->tahun_anggaran}}</td>
										</tr>
										<tr>
											<td>Jenis Belanja</td>
											<td>{{$r->jenis_belanja}}</td>
										</tr>
										<tr>
											<td>Volume</td>
											<td>{{$r->volume}}</td>
										</tr>
										<tr>
											<td>Deskripsi</td>
											<td>{{$r->deskripsi}}</td>
										</tr>
										<?php $i = 1; ?>
										@foreach($r->dana as $dana)
										<tr>
											<td>Sumber Dana ({{$i}})</td>
											<td>{{$dana['sumber_dana']}}</td>
										</tr>
										<tr>
											<td>Pagu ({{$i}})</td>
											<td>{{number_format($dana['pagu'],0,'','.')}}</td>
										</tr>
										<tr>
											<td>Kode MAK ({{$i}})</td>
											<td>{{$dana['mak']}}</td>
										</tr>
										<?php $i++; ?>
										@endforeach
										<tr>
											<td>Metode Pemilihan Penyedia</td>
											<td>{{$r->metode_pemilihan_penyedia}}</td>
										</tr>
									</table>
								</td>
							</tr>
						@endforeach
					</tbody>
					<tfoot>
						<tr>
							<th colspan="11" class="text-center text-middle bg-custom-gray">Total</th>
						</tr>
						<tr>
							<th class="text-center text-middle" colspan="3">Paket</th>
							<th class="text-center text-middle" colspan="2">Pagu</th>
							<th class="text-center text-middle" colspan="2">Paket Tayang</th>
							<th class="text-center text-middle" colspan="2">Paket Belum Tayang</th>
							<th colspan="2"></th>
						</tr>
						<tr>
							<td colspan="3" class="text-center text-middle">{{$stats['total_paket']}}</td>
							<td colspan="2" class="text-center text-middle">
								Rp. {{number_format($stats['total_pagu'],0,'','.')}}
							</td>
							<td colspan="2" class="text-center text-middle">
								{{$stats['paket_tayang']}}
							</td>
							<td colspan="2" class="text-center text-middle">
								{{$stats['paket_belum_tayang']}}
							</td>
							<th colspan="2"></th>
						</tr>
					</tfoot>
				</table>
			</div>
		@else
			<div class="panel-body">
				Record RUP Penyedia tidak ditemukan
			</div>
		@endif
	</div>
@endsection

@section('styles')
<link rel="stylesheet" href="{{asset('css/custom-theme/jquery-ui-1.9.2.custom.css')}}">
<link rel="stylesheet" href="{{asset('css/jqueryui-editable.css')}}">
@endsection

@section('scripts')
<script src="{{asset('js/jquery-ui-1.9.2.custom.min.js')}}"></script>
<script src="{{asset('js/jqueryui-editable.min.js')}}"></script>
<script src="{{asset('js/lelang.js')}}"></script>
<script src="{{asset('js/editable.js')}}"></script>
@endsection