<!DOCTYPE html>
<html lang="en">
    <head>
    	<meta charset="UTF-8">
    	<title>Sipeng - Reset Password</title>
    	<link rel="stylesheet" href="{{asset('css/app.css')}}">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-xs-4 col-xs-offset-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">Reset Password</div>
                        </div>
                        <form class="form-signin" action="{{route('postResetPassword')}}" method="post">
                            <div class="panel-body">
                                <input name="_token" type="hidden" value="{!! csrf_token() !!}">
                                <input name="email" type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
                            </div>
                            <div class="panel-footer">
                                <button class="btn btn-primary btn-block btn-signin" type="submit">Reset Password</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>