@extends('layouts.main')

@section('content')
<div class="panel panel-default panel-tab">
	<div class="panel-heading">
		<ul class="nav nav-tabs">
			<li role="presentation"><a href="{{route('laporan.index')}}">Rekapitulasi Paket Lelang</a></li>
			<li role="presentation" class="active"><a href="{{route('laporan.tayang')}}">Tayang Paket Lelang</a></li>
			<li role="presentation"><a href="{{route('laporan.sirup')}}">Rekapitulasi siRUP</a></li>
		</ul>
	</div>
	<div class="panel-body">
		<div class="pull-right clearfix">
			<a href="{{route('laporan.download.tayang')}}{{($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING']:''}}" class="btn btn-sm btn-success btn-download">
				<i class="fa fa-fw fa-file-excel-o"></i> Unduh
			</a>
		</div>

		{!! Former::populate(Input::get()) !!}
		{!! Former::open_inline(route('laporan.tayang'))->method('get') !!}
		{!! Former::select('tahun')->options($filters['tahun'])->placeholder('Tahun'); !!}
		{!! Former::select('satuan_kerja')->options($filters['satuan_kerja'])->placeholder('Satuan Kerja'); !!}
		<button class="btn btn-sm btn-default"><i class="fa fa-fw fa-filter"></i> Filter</button>
		{!! Former::close() !!}
	</div>
	<div class="table-responsive">
		@include('core::reports._table_tayang')
	</div>
</div>
@endsection

@section('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css">
@stop

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
<script>
$(function(){
	$('select').select2();
})
</script>
@stop