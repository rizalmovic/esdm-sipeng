<table class="table table-bordered table-small">
	<thead>
		<tr>
			<th width="500" style="text-align:center;vertical-align:middle" rowspan="2">Satuan Kerja</th>
			<th style="text-align:center;vertical-align:middle" colspan="3">Rencana</th>
			<th style="text-align:center;vertical-align:middle" colspan="5">Realisasi</th>
			<th style="text-align:center;vertical-align:middle" rowspan="2">Sisa Paket Belum Tayang</th>
		</tr>
		<tr>
			<th style="text-align:center;vertical-align:middle">Paket</th>
			<th style="text-align:center;vertical-align:middle">Pagu</th>
			<th style="text-align:center;vertical-align:middle">HPS</th>
			<th style="text-align:center;vertical-align:middle">Paket Tayang</th>
			<th style="text-align:center;vertical-align:middle">HPS</th>
			<th style="text-align:center;vertical-align:middle">% Paket Tayang</th>
			<th style="text-align:center;vertical-align:middle">HPS / PAGU</th>
			<th style="text-align:center;vertical-align:middle">PAGU - HPS</th>
		</tr>
	</thead>
	<tbody>
		@if($penyedia['count'])
			@foreach($penyedia['retval'] as $s)
			<tr>
				<td>{{ucwords(strtolower($s['satuan_kerja']))}}</td>
				<td align="center">{{$s['total_paket']}}</td>
				<td align="center">{{ number_format($s['total_pagu'],0,'','.') }}</td>
				<td align="center">{{ number_format($s['rencana_hps'],0,'','.') }}</td>
				<td align="center">{{$s['lelang']}}</td>
				<td align="center">{{ number_format($s['total_hps'],0,'','.') }}</td>
				<td align="center">
					{{ number_format(($s['lelang'] / $s['total_paket']) * 100,1,',','.').'%' }}
				</td>
				<td align="center">
					{{ ($s['total_pagu']) ? number_format(($s['total_hps'] / $s['total_pagu']) * 100,1,',','.').'%' : '-' }}
				</td>
				<td align="center">{{ number_format($s['total_pagu'] - $s['total_hps'],0,'','.') }}</td>
				<td align="center">{{$s['belum_lelang']}}</td>
			</tr>
			@endforeach
		@else
			<tr>
				<td colspan="10">Tidak ada data</td>
			</tr>
		@endif
	</tbody>
	<tfoot>
		<tr>
			<th>Total</th>
			<td align="center">
				{{array_sum(array_pluck($penyedia['retval'],'total_paket'))}}
			</td>
			<td align="center">
				{{ number_format(array_sum(array_pluck($penyedia['retval'],'total_pagu')), 0, '','.') }}
			</td>
			<td align="center">
				{{ number_format(array_sum(array_pluck($penyedia['retval'],'rencana_hps')), 0, '','.') }}
			</td>
			<td align="center">
				{{array_sum(array_pluck($penyedia['retval'],'lelang'))}}
			</td>
			<td align="center">
				{{ number_format(array_sum(array_pluck($penyedia['retval'],'total_hps')),0,'','.')}}
			</td>
			<td align="center">
				@if($penyedia['count'])
				{{ number_format((
						array_sum(array_pluck($penyedia['retval'],'lelang')) / array_sum(array_pluck($penyedia['retval'],'total_paket'))
					) * 100,1,',','.').'%'
				}}
				@else
					0 %
				@endif
			</td>
			<td align="center">
				@if($penyedia['count'])
				{{
					(array_sum(array_pluck($penyedia['retval'],'total_pagu'))) ?
						number_format(
							(array_sum(array_pluck($penyedia['retval'],'total_hps')) / array_sum(array_pluck($penyedia['retval'],'total_pagu')) ) * 100,1,',','.'
						).'%' : '-'
				}}
				@else
					0
				@endif
			</td>
			<td align="center">
				@if($penyedia['count'])
					{{ number_format(array_sum(array_pluck($penyedia['retval'],'total_pagu')) - array_sum(array_pluck($penyedia['retval'],'total_hps')),0,'','.') }}
				@else
					0
				@endif
			</td>
			<td align="center">
				{{array_sum(array_pluck($penyedia['retval'],'belum_lelang'))}}
			</td>
		</tr>
	</tfoot>
</table>