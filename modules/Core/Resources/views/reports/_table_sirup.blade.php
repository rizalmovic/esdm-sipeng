<table class="table table-bordered table-small">
	<thead>
		<tr>
			<th rowspan="2" class="text-center text-middle">NO</th>
			<th width="300" rowspan="2" class="text-center text-middle">Satuan Kerja</th>
			<th colspan="2" class="text-center text-middle">Penyedia</th>
			<th colspan="2" class="text-center text-middle">Swakelola</th>
			<th rowspan="2" class="text-center text-middle">Total Paket</th>
			<th rowspan="2" class="text-center text-middle">Total Pagu</th>
		</tr>
		<tr>
			<th class="text-center text-middle">Paket</th>
			<th class="text-center text-middle">Pagu</th>
			<th class="text-center text-middle">Paket</th>
			<th class="text-center text-middle">Pagu</th>
		</tr>
	</thead>
	<tbody>
		@if($result['count'])
			<?php $i = 1 ?>
			@foreach($result['retval'] as $s)
			<tr>
				<td class="text-center text-middle">{{$i}}</td>
				<td class="text-middle">{{ucwords(strtolower($s['satuan_kerja']))}}</td>
				<td class="text-center text-middle">{{$s['lelang']['paket']}}</td>
				<td class="text-right text-middle">
					<div class="pull-left clearfix">Rp.</div>
					{{number_format($s['lelang']['pagu'],0,'','.')}}
				</td>
				<td class="text-center text-middle">{{$s['swakelola']['paket']}}</td>
				<td class="text-right text-middle">
					<div class="pull-left clearfix">Rp.</div>
					{{number_format($s['swakelola']['pagu'],0,'','.')}}
				</td>
				<td class="text-center text-middle">{{$s['total']['paket']}}</td>
				<td class="text-right text-middle">
					<div class="pull-left clearfix">Rp.</div>
					{{number_format($s['total']['pagu'],0,'','.')}}
				</td>
			</tr>
			<?php $i++?>
			@endforeach
		@else
			<tr>
				<td colspan="10">Tidak ada data</td>
			</tr>
		@endif
	</tbody>
</table>