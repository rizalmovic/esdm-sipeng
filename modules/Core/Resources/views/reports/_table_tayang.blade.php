<table class="table table-bordered">
	<thead>
		<tr>
			<th colspan="2" rowspan="2" class="text-center text-middle">Nama Paket</th>
			<th rowspan="2" class="text-center text-middle">Satuan Kerja</th>
			<th colspan="2" class="text-center text-middle">HPS</th>
			<th colspan="2" class="text-center text-middle">Tanggal Lelang</th>
			<th rowspan="2" class="text-center text-middle">PIC</th>
		</tr>
		<tr>
			<th class="text-center text-middle" width="125">Rencana</th>
			<th class="text-center text-middle" width="125">Lelang</th>
			<th class="text-center text-middle">Mulai</th>
			<th class="text-center text-middle">Sampai</th>
		</tr>
	</thead>
	<tbody>
		@foreach($paket as $p)
			<tr>
				<td class="{{ ($p->lpse) ? 'bg-success':'' }}"></td>
				<td>{{$p->paket_nama}}</td>
				<td>{{ucwords(strtolower($p->satuan_kerja))}}</td>
				<td class="text-right text-middle">
					@if($p->hps)
						<div class="pull-left clearfix">Rp.</div>
						{{number_format($p->hps,0,'','.')}}
					@else
						Belum ada data
					@endif
				</td>
				<td class="text-right text-middle">
					@if($p->lpse['hps'])
						<div class="pull-left clearfix">Rp.</div>
						{{number_format($p->lpse['hps'],0,'','.')}}
					@else
						Belum ada data
					@endif
				</td>
				@if($p->lpse['jadwal'])
					<td class="text-center text-middle">
						{{date('d/m/Y', $p->lpse['jadwal']['download_dokumen_pengadaan']['mulai']->sec)}}
					</td>
					<td class="text-center text-middle">
						{{date('d/m/Y', $p->lpse['jadwal']['download_dokumen_pengadaan']['sampai']->sec)}}
					</td>
				@else
					<td colspan="2" class="text-center text-middle">Belum Lelang</td>
				@endif
				<td class="text-center text-middle">{{$p->pic}}</td>
			</tr>
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<th colspan="3" class="text-center text-middle">Total</th>
			<td class="text-right text-middle">
				<div class="pull-left clearfix">Rp.</div>
				{{number_format($paket->sum('hps'),0,'','.')}}
			</td>
			<td class="text-right text-middle">
				<div class="pull-left clearfix">Rp.</div>
				{{number_format($paket->sum('lpse.hps'),0,'','.')}}
			</td>
			<td colspan="3"></td>
		</tr>
	</tfoot>
</table>