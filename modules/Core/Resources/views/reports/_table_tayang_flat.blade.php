<table class="table table-bordered">
	<thead>
		<tr>
			<th class="text-center text-middle">Nama Paket</th>
			<th class="text-center text-middle">Satuan Kerja</th>
			<th class="text-center text-middle">HPS Rencana</th>
			<th class="text-center text-middle">HPS Lelang</th>
			<th class="text-center text-middle">Tanggal Lelang Mulai</th>
			<th class="text-center text-middle">Tanggal Lelang Sampai</th>
			<th class="text-center text-middle">PIC</th>
		</tr>
	</thead>
	<tbody>
		@foreach($paket as $p)
			<tr>
				<td>{{$p->paket_nama}}</td>
				<td>{{ucwords(strtolower($p->satuan_kerja))}}</td>
				<td class="text-right text-middle">
					@if($p->hps)
						<div class="pull-left clearfix">Rp.</div>
						{{number_format($p->hps,0,'','.')}}
					@else
						Belum ada data
					@endif
				</td>
				<td class="text-right text-middle">
					@if($p->lpse['hps'])
						<div class="pull-left clearfix">Rp.</div>
						{{number_format($p->lpse['hps'],0,'','.')}}
					@else
						Belum ada data
					@endif
				</td>
				@if($p->lpse['jadwal'])
					<td class="text-center text-middle">
						{{date('d/m/Y', $p->lpse['jadwal']['download_dokumen_pengadaan']['mulai']->sec)}}
					</td>
					<td class="text-center text-middle">
						{{date('d/m/Y', $p->lpse['jadwal']['download_dokumen_pengadaan']['sampai']->sec)}}
					</td>
				@else
					<td class="text-center text-middle">Belum Lelang</td>
					<td class="text-center text-middle">Belum Lelang</td>
				@endif
				<td class="text-center text-middle">{{$p->pic or '-'}}</td>
			</tr>
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<td colspan="2">Total</td>
			<td>{{number_format($paket->sum('hps'),0,'','.')}}</td>
			<td>{{number_format($paket->sum('lpse.hps'),0,'','.')}}</td>
			<td colspan="3"></td>
		</tr>
	</tfoot>
</table>