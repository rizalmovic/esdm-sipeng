<?php
namespace Modules\Core\Models;

use Cartalyst\Sentinel\Users\EloquentUser;
use LaravelArdent\Ardent\Ardent;

class User extends EloquentUser
{
	protected $fillable = [
		'name',
		'address',
		'phone',
		'email',
		'password',
		'permissions'
	];

	// public function roles()
	// {
	// 	return $this->belongsToMany(\Modules\Core\Models\Roles::class, 'role_users', 'user_id', 'role_id');
	// }
}