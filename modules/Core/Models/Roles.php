<?php
namespace Modules\Core\Models;

use LaravelArdent\Ardent\Ardent;

class Roles  extends Ardent
{
	protected $fillable = [
		'slug',
		'name',
		'permissions',
		'eselon',
		'type',
		'kode_mak'
	];

	public function setKodeMakAttribute($value)
	{
		$this->attributes['kode_mak'] = json_encode(explode(',', strtolower($value)));
	}

	public function getKodeMakAttribute($value)
	{
		return json_decode($value);
	}

	private static function createSlug($slug, $id = null)
    {
        $counter = 0;
        $temp = $slug;
        $doFind = true;

        while($doFind)
        {
            if($id)
            {
                if(!Roles::select(['id'])->where('id','!=', $id)->where('slug',$temp)->get()->count())
                {
                    $doFind = false;
                    break;
                }
                else
                {
                    return $temp;
                }
            }
            else
            {
                if(!Roles::select(['id'])->where('slug',$temp)->get()->count())
                {
                    $doFind = false;
                    break;
                }
                else
                {
                    return $temp;
                }
            }

            $counter++;
            $temp = $slug.'-'.$counter;
        }

        unset($counter, $temp, $id);

        return $slug;
    }

	public function beforeSave()
	{
	    $id = \Input::get('id', false);

	    if(!$this->slug)
	    {
	    	$this->slug = str_slug($this->name);
	    }

        if($id)
        {
            $this->slug = self::createSlug($this->slug, $id);
        }
        else
        {
            $this->slug = self::createSlug($this->slug);
        }
  }
}