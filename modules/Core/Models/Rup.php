<?php
namespace Modules\Core\Models;

use Jenssegers\Mongodb\Model as Eloquent;
use Goutte\Client;
class Rup extends Eloquent
{
	protected $collection = 'rup';
	protected $connection = 'mongodb';
	protected $guarded = [];
	protected $dates = [
		'tanggal_pengumuman',
		'tanggal_awal_pengadaan',
		'tanggal_akhir_pengadaan',
		'tanggal_awal_pekerjaan',
		'tanggal_akhir_pekerjaan',
		'created_at',
		'updated_at'
	];

	public function getPagu()
	{
		$result = 0;
		foreach ($this->dana as $d)
		{
			$result += $d['pagu'];
		}

		return $result;
	}

	// LPSE AGGREGATOR
	public function lpseAggregator($kode_lelang)
	{
		$datas = [];
		$client = new Client();
		$scrap = $client->request('GET', "http://eproc.esdm.go.id/eproc/lelang/view/$kode_lelang");
		$scrap->filter('table > tr td.horizLine')->each(function($node, $i) use (&$datas){
			if(!$node->filter('table')->count())
			{
				$datas[] = $node->text();
			}
		});

		if(!$datas)
			return null;

		$keys = [
			0 => 'kode_lelang',
			1 => 'paket_nama',
			2 => 'keterangan',
			3 => 'tahap',
			4 => 'agency',
			5 => 'satuan_kerja',
			6 => 'kategori',
			7 => 'metode_pemilihan',
			8 => 'metode_dokumen',
			9 => 'metode_kualifikasi',
			10 => 'metode_evaluasi',
			11 => 'anggaran',
			12 => 'pagu',
			13 => 'hps',
			14 => 'cara_pembayaran',
			15 => 'pembebanan_tahun_anggaran',
			16 => 'sumber_pendanaan',
			17 => 'kualifikasi_usaha',
			18 => 'lokasi_pekerjaan'
		];

		$mapped = [];
		foreach ($keys as $k => $v)
		{
			if($k == 12 || $k == 13)
			{
				$datas[$k] = (int) preg_replace("/[a-z.]/i", '', $datas[$k]);
			}

			$mapped[$v] = $datas[$k];
		}

		unset($datas, $keys);
		return $mapped;
	}

	public function lpseScheduleAggregator($kode_lelang)
	{
		$datas = [];
		$client = new \Goutte\Client();
		$scrap = $client->request('GET', "http://eproc.esdm.go.id/eproc/lelang/tahap/5047109");
		$scrap->filter('table tr')->each(function($node, $i) use (&$datas){
			if($i !== 0)
			{
				foreach ($node as $k => $v)
				{
					$k = snake_case(preg_replace("/[\\\\!\@\#\$\%\^\&\*\(\)\-\/]/i", ' ', $node->filter('td')->eq(1)->text()));
					$datas[$k] = [
						'mulai' => $node->filter('td')->eq(2)->text(),
						'sampai' => $node->filter('td')->eq(3)->text(),
						'perubahan' => $node->filter('td')->eq(4)->text()
					];
				}
			}
		});

		foreach ($datas as &$data)
		{
			$data['mulai'] = trim($data['mulai']) ? $this->id_date($data['mulai']) : null;
			$data['sampai'] = trim($data['sampai']) ? $this->id_date($data['sampai']) : null;
		}

		return $datas;
	}

	public function id_date($date)
	{
		$dictionary = [
			'januari' => 1,
			'februari' => 2,
			'maret' => 3,
			'april' => 4,
			'mei' => 5,
			'juni' => 6,
			'juli' => 7,
			'agustus' => 8,
			'september' => 9,
			'oktober' => 10,
			'november' => 11,
			'desember' => 12
		];

		try
		{
			$date = explode(' ',trim($date));
			$date[1] = $dictionary[strtolower($date[1])];
			$date[4] = $date[0].'-'.$date[1].'-'.$date[2];
			$date[5] = new \MongoDate(strtotime($date[4]));
			return $date[5];
		}
		catch (Exception $e)
		{
			return $date;
		}
	}
}