<?php
namespace Modules\Core\Models;

use LaravelArdent\Ardent\Ardent;

class Artikel extends Ardent
{
	protected $table = 'articles';
	protected $guarded = [];
}