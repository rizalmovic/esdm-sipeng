<?php

Route::get('/', ['as' => 'index', 'uses' => 'Modules\Core\Http\Controllers\Dashboard@index']);

Route::group(['namespace' => 'Modules\Core\Http\Controllers'], function()
{
	Route::controller('auth',Auth::class, [
		'getLogin'  => 'login',
		'postLogin' => 'authenticate',
		'getLogout' => 'logout',
		'getProfile' => 'profile',
		'postProfile' => 'update_profile',
		'getResetPassword' => 'getResetPassword',
		'postResetPassword' => 'postResetPassword'
	]);

	Route::controller('laporan', Reports::class, [
		'getIndex' => 'laporan.index',
		'getDownloadIndex' => 'laporan.download.index',
		'getTayangPaketLelang' => 'laporan.tayang',
		'getDownloadTayang' => 'laporan.download.tayang',
		'getSirup' => 'laporan.sirup',
		'getDownloadSirup' => 'laporan.download.sirup',
	]);

	Route::group(['prefix' => 'paket'], function(){
		Route::get('penyedia', ['as' => 'paket.penyedia.rencana', 'uses' => 'Paket@getPaketPenyedia']);
		Route::get('realisasi-penyedia', ['as' => 'paket.penyedia.realisasi', 'uses' => 'Paket@getRealisasiPenyedia']);
		Route::get('swakelola', ['as' => 'paket.swakelola.rencana', 'uses' => 'Paket@getPaketSwakelola']);
		Route::get('realisasi-swakelola', ['as' => 'paket.swakelola.realisasi', 'uses' => 'Paket@getRealisasiSwakelola']);
		Route::post('penyedia', ['as' => 'postPenyedia', 'uses' => 'Paket@postPaketPenyedia']);
		Route::post('delete', ['as' => 'postDelete', 'uses' => 'Paket@postDelete']);
		Route::post('single-column-update', ['as' => 'postSingleColumnUpdate', 'uses' => 'Paket@postSingleColumnUpdate']);
	});

	Route::group(['prefix' => 'artikel'], function(){
		Route::get('/', ['as' => 'artikel.index', 'uses' => 'Artikel@index']);
		Route::get('new', ['as' => 'artikel.create', 'uses' => 'Artikel@create']);
		Route::post('store', ['as' => 'artikel.store', 'uses' => 'Artikel@store']);
		Route::get('view/{id}', ['as' => 'artikel.view', 'uses' => 'Artikel@show']);
		Route::get('edit/{id}', ['as' => 'artikel.edit', 'uses' => 'Artikel@edit']);
		Route::put('update/{id}', ['as' => 'artikel.update', 'uses' => 'Artikel@update']);
		Route::delete('delete/{id}', ['as' => 'artikel.delete', 'uses' => 'Artikel@delete']);
	});

	Route::group(['prefix' => 'api'], function(){
		Route::get('paket-strategis', ['as' => 'getPaketStrategis', 'uses' => 'Api@getPaketStrategis']);
		Route::get('paket-swakelola', ['as' => 'getPaketSwakelola', 'uses' => 'Api@getPaketSwakelola']);
		Route::get('paket-total', ['as' => 'getPaketTotal', 'uses' => 'Api@getTotalPaket']);
		Route::get('test', ['uses' => 'Api@getChartPaketStrategis']);
		Route::get('users', ['as' => 'api.users', function(){
			$users = Modules\Core\Models\User::where('function','=', 'PPK')->get(['name']);
			$result = [];
			foreach ($users as $u){
				$result[] = [
					'value' => $u->name,
					'text'  => ucwords($u->name)
				];
			}
			return \Response::json($result);
		}]);
	});

	Route::group(['prefix' => 'settings', 'middleware' => 'App\Http\Middleware\Admin'], function(){
		Route::controller('satuan_kerja', SatuanKerja::class, [
			'getIndex' => 'satker.index',
			'getCreate' => 'satker.create',
			'postCreate' => 'satker.postCreate',
			'getEdit' => 'satker.edit',
			'putUpdate' => 'satker.update',
			'deleteDelete' => 'satker.delete'
		]);

		Route::controller('pengguna', Pengguna::class, [
			'getIndex' => 'pengguna.index',
			'getCreate' => 'pengguna.create',
			'postCreate' => 'pengguna.postCreate',
			'getEdit' => 'pengguna.edit',
			'putUpdate' => 'pengguna.update',
			'deleteDelete' => 'pengguna.delete',
			'getResetPassword' => 'pengguna.reset'
		]);
	});
});