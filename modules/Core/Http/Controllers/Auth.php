<?php namespace Modules\Core\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use App\Http\Middleware\Authenticate;
use Modules\Core\Models\User;
use Request;
use Hash;
use Mail;

class Auth extends Controller {

	public function __construct()
	{
		// parent::__construct();

		$this->middleware('auth', ['only' => [
			'getProfile',
			'postProfile'
		]]);
	}

	public function getLogin()
	{
		return view('core::login');
	}

	public function postLogin()
	{
		$auth = \Sentinel::authenticate(\Input::all());

		if($auth)
		{
			return redirect()->to('/');
		}
		else
		{
			return redirect()->back();
		}
	}

	public function getLogout()
	{
		\Sentinel::logout();

		return redirect()->to('/');
	}

	public function getProfile()
	{
		$user = \Sentinel::getUser();
		return view('core::profile', compact('user'));
	}

	public function postProfile(Request $req)
	{
		$input = $req::all();
		$user = \Sentinel::getUser();

		if(isset($input['type']) && $input['type'] === 'password')
		{

			if(Hash::check($input['old_password'], $user->password) && $input['password'] == $input['password_confirmation'])
			{
				$user->password = Hash::make($input['password']);
			}
		}
		else
		{
			$user->name = $input['name'];
			$user->email = $input['email'];
		}


		$user->save();

		return redirect()->route('profile');
	}

	public function getResetPassword()
	{
		return view('core::reset');
	}

	public function postResetPassword(Request $req)
	{
		$email = $req::get('email');
		$password = str_random(16);

		$user = User::where('email', $email)->first();

		$user->password = Hash::make($password);
		$user->save();

		Mail::send('core::mails.reset-password', ['user' => $user, 'password' => $password], function($m) use ($user, $password){
			$m->to($user->email, $user->name)
			  ->subject('SIPENG - Password baru telah aktif');
		});

		return redirect()->route('login');
	}

}