<?php namespace Modules\Core\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Modules\Core\Models\User;

class Pengguna extends Base
{
	public function __construct()
	{
		parent::__construct();
		$this->params = [
			'title' => 'Pengguna'
		];
	}

	public function getIndex()
	{
		$users = User::paginate();
		$params = $this->params;
		return view('core::users.index', compact('users','params'));
	}

	public function getCreate()
	{
		$params = $this->params;
		return view('core::users.create', compact('users','params'));
	}

	public function postCreate()
	{
		$input = \Input::except(['_token','_method']);
		$input['password'] = str_random(8);
		$user = \Sentinel::registerAndActivate($input);

		if($user)
		{
			$user->roles()->sync(\Input::get('roles'));
			return redirect()->route('pengguna.index');
		}
		else
		{
			return redirect()->back();
		}
	}

	public function getEdit($id)
	{
		$user = User::with('roles')->where('id',$id)->first();
		$params = $this->params;
		return view('core::users.edit', compact('user','params'));
	}

	public function putUpdate($id)
	{
		$user = User::where('id',$id)->first();
		$update = $user->update(\Input::all());
		if($update)
		{
			$user->roles()->sync(\Input::get('roles'));
			return redirect()->route('pengguna.index');
		}
		else
		{
			return redirect()->back();
		}
	}

	public function deleteDelete($id)
	{
		User::where('id',$id)->first()->delete();
		return redirect()->route('pengguna.index');
	}

	public function getResetPassword($email)
	{
		$password = str_random(16);

		$user = User::where('email', $email)->first();

		$user->password = \Hash::make($password);
		$user->save();

		\Mail::send('core::mails.reset-password', ['user' => $user, 'password' => $password], function($m) use ($user, $password){
			$m->to($user->email, $user->name)
			  ->subject('SIPENG - Password baru telah aktif');
		});
		return redirect()->back();
	}
}