<?php
namespace Modules\Core\Http\Controllers;

use Modules\Core\Models\Rup;

class Api extends Base
{
	public function getPaketStrategis()
	{
		$keys = ['satuan_kerja' => 1];
		$initial = [
			'total_paket' => 0,
			'total_pagu' => 0,
			'selesai' => 0,
			'belum_selesai' => 0,
			'kontrak' => 0,
			'belum_kontrak' => 0
		];
		$reduce = "function(current, result){ result.total_paket++; for(var i = 0;i < current.dana.length;i++){ result.total_pagu += current.dana[i].pagu; } if(current.status_pekerjaan){ result.selesai++; } else { result.belum_selesai++; } if(current.no_kontrak){result.kontrak++;} else {result.belum_kontrak++;} }";
		$options = [
			'condition' => [
				'tipe' => 'PENYEDIA'
			]
		];

		$result = Rup::raw(function($collection) use ($keys, $initial, $reduce, $options){
			return $collection->group($keys, $initial, $reduce, $options);
		});

		return \Response::json($result['retval']);
	}

	public function getPaketSwakelola()
	{
		$keys = ['satuan_kerja' => 1];
		$initial = [
			'total_paket' => 0,
			'total_pagu' => 0,
			'selesai' => 0,
			'belum_selesai' => 0,
			'kontrak' => 0,
			'belum_kontrak' => 0
		];
		$reduce = "function(current, result){ result.total_paket++; for(var i = 0;i < current.dana.length;i++){ result.total_pagu += current.dana[i].pagu; } if(current.status_pekerjaan){ result.selesai++; } else { result.belum_selesai++; } if(current.no_kontrak){result.kontrak++;} else {result.belum_kontrak++;} }";
		$options = [
			'condition' => [
				'tipe' => 'SWAKELOLA'
			]
		];

		$result = Rup::raw(function($collection) use ($keys, $initial, $reduce, $options){
			return $collection->group($keys, $initial, $reduce, $options);
		});

		return \Response::json($result['retval']);
	}

	public function getTotalPaket()
	{
		$keys = ['satuan_kerja' => 1];
		$initial = [
			'total_paket' => 0,
			'total_pagu' => 0,
			'selesai' => 0,
			'belum_selesai' => 0,
			'kontrak' => 0,
			'belum_kontrak' => 0
		];
		$reduce = "function(current, result){ result.total_paket++; for(var i = 0;i < current.dana.length;i++){ result.total_pagu += current.dana[i].pagu; } if(current.status_pekerjaan){ result.selesai++; } else { result.belum_selesai++; } if(current.no_kontrak){result.kontrak++;} else {result.belum_kontrak++;} }";

		$result = Rup::raw(function($collection) use ($keys, $initial, $reduce){
			return $collection->group($keys, $initial, $reduce);
		});

		return \Response::json($result['retval']);
	}

	public function getChartPaketStrategis()
	{
		$keys = ['satuan_kerja' => 1];
		$initial = [
			'total_paket' => 0,
			'total_pagu' => 0,
			'selesai' => 0,
			'belum_selesai' => 0,
			'kontrak' => 0,
			'belum_kontrak' => 0
		];
		$reduce = "function(current, result){ result.total_paket++; for(var i = 0;i < current.dana.length;i++){ result.total_pagu += current.dana[i].pagu; } if(current.status_pekerjaan){ result.selesai++; } else { result.belum_selesai++; } if(current.no_kontrak){result.kontrak++;} else {result.belum_kontrak++;} }";
		$options = [
			'condition' => [
				'tipe' => 'PENYEDIA',
				'metode_pemilihan_penyedia' => [
					"$not" => "e-Purchasing"
				]
			]
		];

		$result = Rup::raw(function($collection) use ($keys, $initial, $reduce, $options){
			return $collection->group($keys, $initial, $reduce, $options);
		});

		return \Response::json($result['retval']);
	}
}


