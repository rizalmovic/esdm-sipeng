<?php namespace Modules\Core\Http\Controllers;

use Modules\Core\Repositories\Roles;
class SatuanKerja extends Base
{
	public function __construct()
	{
		parent::__construct();
		$this->params = [
			'title' => 'Satuan Kerja'
		];
	}

	public function getIndex()
	{
		$roles = Roles::getStructuralRoles();
		$params = $this->params;
		return view('core::satker.index', compact('roles', 'params'));
	}

	public function getCreate()
	{
		$params = $this->params;
		return view('core::satker.create', compact('params'));
	}

	public function postCreate()
	{
		$input = \Input::except(['_token','_method']);

		$input['type'] = 'structural';
		$input['permissions'] = '';

		$new = Roles::insertStructuralRole($input);

		if($new)
		{
			return redirect()->route('satker.index');
		}
		else
		{
			return redirect()->back();
		}
	}

	public function getEdit($id)
	{
		$params = $this->params;
		$role = Roles::getRoleById($id);
		return view('core::satker.edit', compact('params', 'role'));
	}

	public function putUpdate($id)
	{
		$update = Roles::update($id, \Input::all());

		if($update)
		{
			return redirect()->route('satker.index');
		}
		else
		{
			return redirect()->back();
		}
	}

	public function deleteDelete($id)
	{
		Roles::delete($id);
		return redirect()->route('satker.index');
	}
}