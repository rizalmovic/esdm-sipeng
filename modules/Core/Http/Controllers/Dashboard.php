<?php
namespace Modules\Core\Http\Controllers;

use Modules\Core\Models\Rup;
use Modules\Core\Models\Lpse;

class Dashboard extends Base
{
	public function index()
	{
		$input = \Input::all();
		$year = (isset($input['tahun']) && $input['tahun']) ? $input['tahun'] : date('Y', strtotime('now'));
		$keys = ['satuan_kerja' => 1];
		$initial = [
			'lelang' => 0,
			'belum_lelang' => 0
		];
		$reduce = "function(current, result){ if(current.kode_lelang){result.lelang++;} else {result.belum_lelang++;} }";
		$options = [
			'condition' => [
				'tipe' => 'PENYEDIA',
				'metode_pemilihan_penyedia' => [
					'$ne' => 'e-Purchasing'
				],
				'tahun_anggaran' => $year
			]
		];

		$penyedia = Rup::raw(function($collection) use ($keys, $initial, $reduce, $options){
			return $collection->group($keys, $initial, $reduce, $options);
		});

		$lelang['pie_lelang'] = [
			[
				'value' => 0,
				'color' => "#46BFBD",
		        'highlight' => "#5AD3D1",
		        'label' => "Telah lelang"
			],
			[
				'value' => 0,
				'color' =>"#F7464A",
        		'highlight' => "#FF5A5E",
		        'label' => "Belum lelang"
			]
		];

		foreach ($penyedia['retval'] as $v)
		{
			$lelang['pie_lelang'][0]['value'] += $v['lelang'];
			$lelang['pie_lelang'][1]['value'] += $v['belum_lelang'];
		}

		return view('core::index', compact('penyedia','lelang'));
	}
}