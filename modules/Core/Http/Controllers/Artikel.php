<?php
namespace Modules\Core\Http\Controllers;

use Input;
use Sentinel;
use Modules\Core\Models\Artikel as Model;
class Artikel extends Base
{
	public $params;

	public function __construct()
	{
		$this->params['title'] = 'Kelola - Artikel';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$pencarian = Input::get('pencarian',false);

		if($pencarian)
		{
			$data = Model::where('judul','like','%'.$pencarian.'%')->paginate(10);
		}
		else
		{
			$data = Model::latest()->paginate(10);
		}

		return view('core::artikel.index', ['data' => $data, 'params' => $this->params ]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('core::artikel.create', ['params' => $this->params]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		//$this->validate($request, ['name' => 'required']); // Uncomment and modify if you need to validate any input.
		$data = Model::create($request->all());
		return redirect()->route('core::artikel.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data = Model::findOrFail($id);
		return view('core::artikel.show', compact('data'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data = Model::findOrFail($id);
		return view('core::artikel.edit', ['data' => $data, 'params' => $this->params]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		//$this->validate($request, ['name' => 'required']); // Uncomment and modify if you need to validate any input.
		$data = Model::findOrFail($id);
		$data->update($request->all());
		return redirect()->route('core::artikel.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data = Model::destroy($id);
		return redirect()->route('core::artikel.index');
	}

}
