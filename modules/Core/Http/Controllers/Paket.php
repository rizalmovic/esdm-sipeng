<?php
namespace Modules\Core\Http\Controllers;

use Modules\Core\Models\Rup;
use Modules\Core\Models\Lpse;

class Paket extends Base
{
	public function getPaketPenyedia()
	{
		$this->params['title'] = 'Penyedia';
		$rup = Rup::where('tipe', '=' ,'PENYEDIA');
		$filters = $this->checkFilter();
		$kegiatan = $this->getKegiatan();


		if($kegiatan)
		{
			$rup = $rup->whereIn('dana.mak', $kegiatan);
		}

		if($filters)
		{
			if(isset($filters['search']) && $filters['search'])
			{
				$rup = $rup->where('paket_nama', 'regex', '/.*'.$filters['search'].'.*/i');

				unset($filters['search']);
			}

			if(isset($filters['metode_pemilihan_penyedia']) && $filters['metode_pemilihan_penyedia'])
			{
				$rup = $rup->where('metode_pemilihan_penyedia', 'regex', '/.*'.$filters['metode_pemilihan_penyedia'].'.*/i');
				unset($filters['metode_pemilihan_penyedia']);
			}

			$rup = $rup->where($filters);
		}
		$rup = $rup->orderBy('tanggal_awal_pengadaan', 'asc')->get();

		// STATS
		$stats['total_paket'] = $rup->count();
		$stats['total_pagu'] = $rup->reduce(function($i, $item){
			return $i + array_sum(array_pluck($item->dana,'pagu'));
		});
		$stats['paket_tayang'] = $rup->reduce(function($i, $item){
			if($item->kode_lelang){
				return $i + 1;
			}
			return $i;
		});
		$stats['paket_belum_tayang'] = $stats['total_paket'] - $stats['paket_tayang'];

		return view('core::paket.penyedia', ['params' => $this->params, 'rup' => $rup, 'stats' => $stats]);
	}

	public function getRealisasiPenyedia()
	{
		$this->params['title'] = 'Penyedia';
		$rup = Rup::where('tipe', '=' ,'PENYEDIA')
				->where(function($q){
					$q->where('lpse', 'exists', true)
					  ->orWhere('metode_pemilihan_penyedia','!=','e-Purchasing');
				});
		$filters = $this->checkFilter();
		$kegiatan = $this->getKegiatan();

		if($kegiatan)
		{
			$rup = $rup->whereIn('dana.mak', $kegiatan);
		}

		if($filters)
		{
			if(isset($filters['search']) && $filters['search'])
			{
				$rup = $rup->where('paket_nama', 'regex', '/.*'.$filters['search'].'.*/i');
				unset($filters['search']);
			}

			if(isset($filters['metode_pemilihan_penyedia']) && $filters['metode_pemilihan_penyedia'])
			{
				$rup = $rup->where('metode_pemilihan_penyedia', 'regex', '/.*'.$filters['metode_pemilihan_penyedia'].'.*/i');
				unset($filters['metode_pemilihan_penyedia']);
			}

			$rup = $rup->where($filters);
		}
		$rup = $rup->orderBy('tanggal_awal_pengadaan', 'asc')->get();

		// STATS
		$stats['total_paket'] = $rup->count();
		$stats['total_pagu'] = $rup->reduce(function($i, $item){
			return $i + array_sum(array_pluck($item->dana,'pagu'));
		});
		$stats['paket_tayang'] = $rup->reduce(function($i, $item){
			if($item->kode_lelang){
				return $i + 1;
			}
			return $i;
		});
		$stats['paket_belum_tayang'] = $stats['total_paket'] - $stats['paket_tayang'];

		return view('core::paket.realisasi_penyedia', ['params' => $this->params, 'rup' => $rup, 'stats' => $stats]);
	}

	public function getPaketSwakelola()
	{
		$this->params['title'] = 'Swakelola';
		$rup = Rup::where('tipe','SWAKELOLA');
		$filters = $this->checkFilter();
		$kegiatan = $this->getKegiatan();

		if($kegiatan)
		{
			$rup = $rup->whereIn('dana.mak', $kegiatan);
		}

		if($filters)
		{
			if(isset($filters['search']) && $filters['search'])
			{
				$rup = $rup->where('kegiatan', 'regex', '/.*'.$filters['search'].'.*/i');

				unset($filters['search']);
			}

			$rup = $rup->where($filters);
		}
		$rup = $rup->orderBy('tanggal_awal_pekerjaan', 'asc')->get();
		return view('core::paket.swakelola', ['params' => $this->params, 'rup' => $rup]);
	}

	public function getRealisasiSwakelola()
	{
		$this->params['title'] = 'Swakelola';
		$rup = Rup::where('tipe','SWAKELOLA');
		$filters = $this->checkFilter();
		$kegiatan = $this->getKegiatan();

		if($kegiatan)
		{
			$rup = $rup->whereIn('dana.mak', $kegiatan);
		}

		if($filters)
		{
			if(isset($filters['search']) && $filters['search'])
			{
				$rup = $rup->where('kegiatan', 'regex', '/.*'.$filters['search'].'.*/i');

				unset($filters['search']);
			}

			$rup = $rup->where($filters);
		}
		$rup = $rup->orderBy('tanggal_awal_pekerjaan', 'asc')->get();
		return view('core::paket.realisasi_swakelola', ['params' => $this->params, 'rup' => $rup]);
	}

	private function checkFilter()
	{
		$filters = false;
		$keys = [
			'satuan_kerja',
			'tahun_anggaran',
			'metode_pemilihan_penyedia',
			'search'
		];

		$result = \Input::only($keys, false);

		if(isset($result['satuan_kerja'])) {
			$result['satuan_kerja'] = strtoupper(str_replace('_', ' ', $result['satuan_kerja']));
		}

		if(isset($result['metode_pemilihan_penyedia'])) {
			$result['metode_pemilihan_penyedia'] = strtoupper(str_replace('_', ' ', $result['metode_pemilihan_penyedia']));
		}

		foreach ($result as $k => $v)
		{
			if($v)
				$filters = true;
			else
				unset($result[$k]);
		}

		if($filters)
			return $result;
		else
			return false;
	}

	public function postSingleColumnUpdate()
	{
		$input = \Input::all();
		$rup = Rup::find($input['pk']);

		if($rup)
		{
			if(!$input['value'])
			{
				$input['value'] = NULL;
			}
			elseif($input['name'] == 'hps' || $input['name'] == 'nilai_kontrak')
			{
				try
				{
					$input['value'] = (int) preg_replace("/[a-z\.\, ]/i", '', $input['value']);
				}
				catch (Exception $e) {}
			}
			elseif($input['name'] == 'tanggal_awal_kontrak' || $input['name'] == 'tanggal_akhir_kontrak')
			{
				try
				{
					$input['value'] = new \MongoDate(strtotime($input['value']));
				}
				catch (Exception $e) {}
			}

			$rup->$input['name'] = $input['value'];

			if($rup->save())
			{
				return \Response::json(['status' => 'Ok'], 200);
			}
			else
			{
				return \Response::json(['status' => 'Terjadi kesalahan dalam melakukan pembaruan data.', 400]);
			}
		}
		else
		{
			return \Response::json(['status' => 'Data tidak ditemukan.', 400]);
		}
	}

	public function postPaketPenyedia()
	{
		$input = \Input::except(['_token','_method']);

		if($input['tipe'] == 'PENYEDIA')
		{
			$input['tanggal_pengumuman'] = date('Y-m-d 00:00:00', strtotime($input['tanggal_pengumuman']));
			$input['tanggal_awal_pengadaan'] = date('Y-m-d 00:00:00', strtotime($input['tanggal_awal_pengadaan']));
			$input['tanggal_akhir_pengadaan'] = date('Y-m-d 00:00:00', strtotime($input['tanggal_akhir_pengadaan']));
		}

		$input['tanggal_awal_pekerjaan'] = date('Y-m-d 00:00:00', strtotime($input['tanggal_awal_pekerjaan']));
		$input['tanggal_akhir_pekerjaan'] = date('Y-m-d 00:00:00', strtotime($input['tanggal_akhir_pekerjaan']));

		// check paket_id if exsists
		$q = Rup::where('paket_id', $input['paket_id'])->first();

		if($q)
		{
			$q->update($input);
		}
		else
		{
			$q = Rup::create($input);
		}

		if($q)
		{
			return \Response::json(['status' => 'OK']);
		}
		else
		{
			return \Response::json(['status' => 'ERROR'], 500);
		}
	}

	public function postDelete()
	{
		$ids = \Input::get('ids');
		$tahun = \Input::get('tahun');
		$kode_satker = \Input::get('kode_satker');
		$tipe = \Input::get('tipe');

		$delete = Rup::whereNotIn('paket_id',$ids)
						->where('tahun_anggaran', $tahun)
						->where('kode_satker', $kode_satker)
						->where('tipe', $tipe)->delete();

		if($delete)
			return \Response::json(['status' => 'FINISHED!']);
		else
			return \Response::json(['status' => 'ERROR'], 500);
	}

	private function getKegiatan()
	{
		$temp = [];

		if(! \Sentinel::check())
		{
			$temp = [];
			// $roles = \DB::table('roles')->get(['kode_mak']);
			// $roles = array_pluck($roles, 'kode_mak');
			// foreach ($roles as $r)
			// {
			// 	if($r)
			// 	{
			// 		$temp = array_merge($temp, json_decode($r));
			// 	}
			// }
		}
		else
		{
			$user = \Sentinel::getUser()->roles()->get(['kode_mak']);
			$mak = array_pluck($user,'kode_mak');

			foreach ($mak as $m)
			{
				if($m)
				{
					$temp = array_merge($temp, json_decode($m));
				}
			}
		}

		foreach ($temp as &$t)
		{
			$t = new \MongoRegex('/'.$t.'/');
		}

		return $temp;
	}
}