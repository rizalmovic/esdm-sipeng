<?php namespace Modules\Core\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Modules\Core\Models\Rup;

class Reports extends Base
{
	public function __construct()
	{
		parent::__construct();
		$this->params = [
			'title' => 'Laporan'
		];
	}

	private function flatter($collection)
	{
		$temp = [];
		$temp[''] = 'Semua';

		foreach ($collection as $c)
		{
			$temp[$c[0]] = ucwords(strtolower($c[0]));
		}

		return $temp;
	}

	public function getIndex()
	{
		$input = \Input::all();
		$filters['tahun'] = $this->flatter(Rup::distinct('tahun_anggaran')->get());

		$year = (isset($input['tahun']) && $input['tahun']) ? $input['tahun'] : date('Y', strtotime('+1 year'));
		$keys = ['satuan_kerja' => 1];
		$initial = [
			'total_paket' => 0,
			'total_pagu' => 0,
			'total_hps' => 0,
			'rencana_hps' => 0,
			'lelang' => 0,
			'belum_lelang' => 0
		];
		$reduce = "function(current, result){ result.total_paket++; for(var i = 0;i < current.dana.length;i++){ result.total_pagu += current.dana[i].pagu; } if(current.kode_lelang){result.lelang++;} else {result.belum_lelang++;} if(current.lpse){result.total_hps += current.lpse.hps;} if(current.hps){result.rencana_hps += current.hps;} }";
		$options = [
			'condition' => [
				'tipe' => 'PENYEDIA',
				'metode_pemilihan_penyedia' => [
					'$ne' => 'e-Purchasing'
				],
				'tahun_anggaran' => $year
			]
		];

		$penyedia = Rup::raw(function($collection) use ($keys, $initial, $reduce, $options){
			return $collection->group($keys, $initial, $reduce, $options);
		});

		return view('core::reports.index', compact('penyedia','filters'));
	}

	public function getTayangPaketLelang()
	{
		$input = \Input::all();
		$filters['tahun'] = $this->flatter(Rup::distinct('tahun_anggaran')->get());
		$filters['satuan_kerja'] = $this->flatter(Rup::distinct('satuan_kerja')->get());

		$paket = Rup::where(function($q){
			$q->where('tipe', 'PENYEDIA')
			  ->where('metode_pemilihan_penyedia','!=','e-Purchasing');
		});

		if(isset($input['tahun']) && $input['tahun'])
		{
			$paket = $paket->where('tahun_anggaran', $input['tahun']);
		}

		if(isset($input['satuan_kerja']) && $input['satuan_kerja'])
		{
			$paket = $paket->where('satuan_kerja', $input['satuan_kerja']);
		}

		$paket = $paket->orderBy('paket_id','asc')->get();

		return view('core::reports.tayang', compact('paket','filters'));
	}

	public function getSirup()
	{
		$input = \Input::all();
		$filters['tahun'] = $this->flatter(Rup::distinct('tahun_anggaran')->get());

		$year = (isset($input['tahun']) && $input['tahun']) ? $input['tahun'] : date('Y', strtotime('+1 year'));
		$keys = ['satuan_kerja' => 1];
		$initial = [
			'lelang' => [ 'paket' => 0, 'pagu' => 0 ],
			'swakelola' => [ 'paket' => 0, 'pagu' => 0 ],
			'total' => [ 'paket' => 0, 'pagu' => 0 ]
		];
		$reduce = "function(current, result){
			if(current.tipe == 'PENYEDIA'){
				for(var i = 0; i < current.dana.length;i++){
					result.lelang.pagu += current.dana[i].pagu;
					result.total.pagu += current.dana[i].pagu;
				}
				result.lelang.paket++;
			} else {
				for(var i = 0; i < current.dana.length;i++){
					result.swakelola.pagu += current.dana[i].pagu;
					result.total.pagu += current.dana[i].pagu;
				}
				result.swakelola.paket++;
			}
			result.total.paket++;
		}";

		$options = [
			'condition' => [
				'tahun_anggaran' => $year
			]
		];

		$result = Rup::raw(function($collection) use ($keys, $initial, $reduce, $options){
			return $collection->group($keys, $initial, $reduce, $options);
		});

		return view('core::reports.sirup', compact('result','filters'));
	}

	public function getDownloadIndex()
	{
		$filters['tahun'] = $this->flatter(Rup::distinct('tahun_anggaran')->get());
		$filters['tipe'] = $this->flatter(Rup::distinct('tipe')->get());

		$year = date('Y', strtotime('+1 year'));
		$keys = ['satuan_kerja' => 1];
		$initial = [
			'total_paket' => 0,
			'total_pagu' => 0,
			'total_hps' => 0,
			'lelang' => 0,
			'belum_lelang' => 0
		];
		$reduce = "function(current, result){ result.total_paket++; for(var i = 0;i < current.dana.length;i++){ result.total_pagu += current.dana[i].pagu; } if(current.kode_lelang){result.lelang++;} else {result.belum_lelang++;} if(current.hps){result.total_hps += current.hps;} }";
		$options = [
			'condition' => [
				'tipe' => 'PENYEDIA',
				'tahun_anggaran' => $year
			]
		];

		$penyedia = Rup::raw(function($collection) use ($keys, $initial, $reduce, $options){
			return $collection->group($keys, $initial, $reduce, $options);
		});

		\Excel::create('laporan.rekap-'.date('dd-mm-yyyy', strtotime('now')), function($excel) use ($penyedia) {
			$excel->sheet('Rekap', function($sheet) use ($penyedia) {
				$sheet->loadView('core::reports._table_rekap')->with('penyedia', $penyedia);
			});
		})->download('xlsx');
	}

	public function getDownloadTayang()
	{
		$input = \Input::all();
		$filters['tahun'] = $this->flatter(Rup::distinct('tahun_anggaran')->get());
		$filters['satuan_kerja'] = $this->flatter(Rup::distinct('satuan_kerja')->get());

		$paket = Rup::where(function($q){
			$q->where('tipe', 'PENYEDIA')
			  ->where('metode_pemilihan_penyedia','!=','e-Purchasing');
		});

		if(isset($input['tahun']) && $input['tahun'])
		{
			$paket = $paket->where('tahun_anggaran', $input['tahun']);
		}

		if(isset($input['satuan_kerja']) && $input['satuan_kerja'])
		{
			$paket = $paket->where('satuan_kerja', $input['satuan_kerja']);
		}

		$paket = $paket->orderBy('paket_id','asc')->get();

		\Excel::create('laporan.tayang-'.date('dd-mm-yyyy', strtotime('now')), function($excel) use ($paket) {
			$excel->sheet('Tayang Lelang', function($sheet) use ($paket) {
				$sheet->loadView('core::reports._table_tayang_flat')->with('paket', $paket);
			});
		})->download('xlsx');
	}

	public function getDownloadSirup()
	{
		$input = \Input::all();
		$filters['tahun'] = $this->flatter(Rup::distinct('tahun_anggaran')->get());

		$year = (isset($input['tahun']) && $input['tahun']) ? $input['tahun'] : date('Y', strtotime('+1 year'));
		$keys = ['satuan_kerja' => 1];
		$initial = [
			'lelang' => [ 'paket' => 0, 'pagu' => 0 ],
			'swakelola' => [ 'paket' => 0, 'pagu' => 0 ],
			'total' => [ 'paket' => 0, 'pagu' => 0 ]
		];
		$reduce = "function(current, result){
			if(current.tipe == 'PENYEDIA'){
				for(var i = 0; i < current.dana.length;i++){
					result.lelang.pagu += current.dana[i].pagu;
					result.total.pagu += current.dana[i].pagu;
				}
				result.lelang.paket++;
			} else {
				for(var i = 0; i < current.dana.length;i++){
					result.swakelola.pagu += current.dana[i].pagu;
					result.total.pagu += current.dana[i].pagu;
				}
				result.swakelola.paket++;
			}
			result.total.paket++;
		}";

		$options = [
			'condition' => [
				'tahun_anggaran' => $year
			]
		];

		$result = Rup::raw(function($collection) use ($keys, $initial, $reduce, $options){
			return $collection->group($keys, $initial, $reduce, $options);
		});

		\Excel::create('laporan.sirup-'.date('dd-mm-yyyy', strtotime('now')), function($excel) use ($result) {
			$excel->sheet('Rekapitulasi Sirup', function($sheet) use ($result) {
				$sheet->loadView('core::reports._table_sirup')->with('result', $result);
			});
		})->download('xlsx');
	}
}