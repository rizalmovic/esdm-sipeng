<?php namespace Modules\Core\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class Base extends Controller
{
	public $params;
	public function __construct()
	{
		$this->params = [
			'title' => 'Default Title'
		];
	}
}