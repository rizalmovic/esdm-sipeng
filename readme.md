## SIPENG - ESDM

Sipeng adalah aplikasi monitoring tahapan paket lelang/swakelola pada organisasi pemerintahan.

## SIRUP

- URL PAGE Satker => https://sirup.lkpp.go.id/sirup/rup/penyedia/satker/{kode}

### LELANG
- URL DATA Index Paket Lelang => https://sirup.lkpp.go.id/sirup/datatablectr/dataruppenyediasatker?tahun={2016}&idSatker={5803}&iDisplayLength={100}

### SWAKELOLA
- URL DATA Index Paket Swakelola => https://sirup.lkpp.go.id/sirup/datatablectr/datarupswakelolasatker?tahun={2016}&idSatker={5803}&iDisplayLength={100}

## LPSE

- URL Search Paket : 
- URL Detil Paket : http://eproc.esdm.go.id/eproc/lelang/view/5297109

## MASTER DATA

### SIRUP

- 5797 : Pusat Penelitian dan Pengembangan Teknologi Minyak dan Gas Bumi "LEMIGAS"
- 5798 : Sekretariat Badan Penelitian dan Pengembangan Energi dan Sumber Daya Mineral
- 5799 : Pusat Penelitian dan Pengembangan Teknologi Ketenagalistrikan, Energi Baru, Terbarukan dan Konservasi Energi
- 5800 : Pusat Penelitian dan Pengembangan Teknologi Mineral dan Batubara
- 5801 : Pusat Penelitian dan Pengembangan Geologi Kelautan

## Scrapper

### Penyedia

./../node_modules/.bin/phantomjs --ignore-ssl-errors=true ./../sirup-penyedia.js 5797 2016

./../node_modules/.bin/phantomjs --ignore-ssl-errors=true ./../sirup-penyedia.js 5798 2016

./../node_modules/.bin/phantomjs --ignore-ssl-errors=true ./../sirup-penyedia.js 5799 2016

./../node_modules/.bin/phantomjs --ignore-ssl-errors=true ./../sirup-penyedia.js 5800 2016

./../node_modules/.bin/phantomjs --ignore-ssl-errors=true ./../sirup-penyedia.js 5801 2016

### Swakelola

./../node_modules/.bin/phantomjs --ignore-ssl-errors=true ./../sirup-swakelola.js 5797 2016 "Kementerian Energi Dan Sumber Daya Mineral" "PUSAT PENELITIAN DAN PENGEMBANGAN TEKNOLOGI MINYAK DAN GAS BUMI LEMIGAS JAKARTA"

./../node_modules/.bin/phantomjs --ignore-ssl-errors=true ./../sirup-swakelola.js 5798 2016 "Kementerian Energi Dan Sumber Daya Mineral" "SEKRETARIAT BADAN PENELITIAN DAN PENGEMBANGAN ENERGI DAN SUMBER DAYA MINERAL"

./../node_modules/.bin/phantomjs --ignore-ssl-errors=true ./../sirup-swakelola.js 5799 2016 "Kementerian Energi Dan Sumber Daya Mineral" "PUSAT PENELITIAN DAN PENGEMBANGAN TEKNOLOGI KETENAGALISTRIKAN DAN ENERGI BARU TERBARUKAN"

./../node_modules/.bin/phantomjs --ignore-ssl-errors=true ./../sirup-swakelola.js 5800 2016 "Kementerian Energi Dan Sumber Daya Mineral" "PUSAT PENELITIAN DAN PENGEMBANGAN TEKNOLOGI MINERAL DAN BATUBARA"

./../node_modules/.bin/phantomjs --ignore-ssl-errors=true ./../sirup-swakelola.js 5801 2016 "Kementerian Energi Dan Sumber Daya Mineral" "PUSAT PENELITIAN DAN PENGEMBANGAN GEOLOGI KELAUTAN"
